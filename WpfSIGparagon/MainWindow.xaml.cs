﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using Microsoft.Win32; // to openFileDialog
using OfficeOpenXml; // EPPlus.dll to create Excel 



namespace WpfSIGparagon
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, IDisposable
    {
        #region Fields and Properties
        private System.Data.DataSet ds = new System.Data.DataSet();
        private System.Data.DataTable dt = new System.Data.DataTable();
        private string csvFilePath = String.Empty; // For uptade DB
        private string excel2kmzFilePath = String.Empty; // For saving kmz file into
        private List<List<List<string>>> excel2kmzList  = new List<List<List<string>>>();
        #endregion

        #region Constructor
        public MainWindow()
        {
            InitializeComponent();
            cmbColors.ItemsSource = typeof(Colors).GetProperties(); // system colors
            cmbColors2.ItemsSource = typeof(Colors).GetProperties();
            cmbColors3.ItemsSource = typeof(Colors).GetProperties();
            ChangeSearchFrameVisibility(false);
        }
        #endregion

        #region Button Clicks
        private void xlsParagon_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "Ficheiros Excel (*.xls, *.xlsx)|*.xls; *.xlsx|Todos os ficheiros|*.*"; 

            if (ofd.ShowDialog() == true)
            {
                String paragonExcelPath = ofd.FileName.ToString();
                string safeFilePath = ofd.SafeFileName;
                XLStext.Text = "Ficheiro carregado com sucesso!"; // VERIFICAR PRIMEIRO QUE É UM FICHEIRO PARAGON VÁLIDO

                /* DEBUG -----------------------------------------------------------------
                var listOfLists = DaoReadXLS.readExcel(paragonExcelPath);
                var list = new List<string>();
                foreach (List<string> lst in listOfLists)
                {
                    list.Add(String.Join(", ", lst));
                }
                var message = String.Join(Environment.NewLine, list);
                MessageBox.Show(message);
                // ------------------------------------------------------------------------- */

                ChangeKmzPropertiesFrameVisibility(true);
            }
        }

        private void csvImport_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "Ficheiros Excel (*.csv)|*.csv;|Todos os ficheiros|*.*";

            if (ofd.ShowDialog() == true)
            {
                csvFilePath = ofd.FileName.ToString();
                UpdateDBLog.Text += "\nFicheiro carregado com sucesso! Clique em 'Actualizar BD'";                
            }
            else
            {
                UpdateDBLog.Text += "\nFicheiro inválido!";
            }
        }

        private void UpdateDbBt_Click(object sender, RoutedEventArgs e)
        {
            UpdateDB update = new UpdateDB();
            string results = update.updateDB(csvFilePath);
            if (!string.IsNullOrEmpty(results))
            {
                UpdateDBLog.Text += "\n" + results;
            }
            else
            {
                UpdateDBLog.Text += "\n Base de Dados actualizado com sucesso";
            }
        }

        private void kmzProperties_Click(object sender, RoutedEventArgs e)
        {
            string message = "Propriedades carregadas!";
            ChangeKmzSaveFrameVisibility(true);
            
            MessageBox.Show(message);
        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            // ?????????????????????
        }

        private void SaveKMZ_Click(object sender, RoutedEventArgs e)
        {            
            //TODO
            MessageBox.Show("Gravado com sucesso!");
        }

        private void ViewQuery_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                // Reset the DataSet
                ds.Reset();

                // Filling Dataset with results from NpgsqlDataAdapter
                List<string> specialColumns = new List<string>(); 
                string sql = GetQueryValues("normal", specialColumns);
                //MessageBox.Show(sql);
                DaoDB.GetQueryResults(sql).Fill(ds);

                // since it c# DataSet can handle multiple tables, we will select first ?????????????
                dt = ds.Tables[0];

                // connect grid to DataTable
                QueryResultsData.ItemsSource = dt.DefaultView;

                // close the connection
                DaoDB.CloseDbConnection();

                QueryResultsData.Visibility = System.Windows.Visibility.Visible;

                string results = dt.Rows.Count.ToString();
                MessageBox.Show(results + " resultados");

                // Clean Values for the next Query
                CleanQueryValues();
            }
            catch (Exception msg)
            {
                // something went wrong, and you wanna know why
                MessageBox.Show(msg.ToString());
                throw;
            }
            ResetSearchDbFrameFields();
            
        }

        private void CleanQueryValues()
        {
            // Clean Values for the next Query
            CgiTx.Text = string.Empty;
            OperadoraTx.Text = string.Empty;
            LacTx.Text = string.Empty;
            CidTx.Text = string.Empty;

            GsmCb.IsChecked = false;
            UmtsCb.IsChecked = false;
            LteCb.IsChecked = false;

            // falta limpar datas

            CpTb.Text = string.Empty;
            LocalTb.Text = string.Empty;
            ConcelhoTx.Text = string.Empty;
            DistritoTx.Text = string.Empty;

            CirLatTx.Text = string.Empty;
            CirLonTx.Text = string.Empty;
            CirRaioTx.Text = string.Empty;

            QuadLat1Tx.Text = string.Empty;
            QuadLon1Tx.Text = string.Empty;
            QuadLat2Tx.Text = string.Empty;
            QuadLon2Tx.Text = string.Empty;
        }

        private void ResetSearchDbFrameFields()
        {
            OperadoraTx.Text = string.Empty;
            LacTx.Text = string.Empty;
            CidTx.Text = string.Empty;
            CgiTx.Text = string.Empty;

            GsmCb.IsChecked = false;
            UmtsCb.IsChecked = false;
            LteCb.IsChecked = false;

            CirLatTx.Text = string.Empty;
            CirLonTx.Text = string.Empty;
            CirRaioTx.Text = string.Empty;

            QuadLat1Tx.Text = string.Empty;
            QuadLon1Tx.Text = string.Empty;
            QuadLat2Tx.Text = string.Empty;
            QuadLon2Tx.Text = string.Empty;

            CpTb.Text = string.Empty;
            LocalTb.Text = string.Empty;
            ConcelhoTx.Text = string.Empty;
            DistritoTx.Text = string.Empty;

            CellActuaisCb.IsChecked = true;

            IconNumTx.Text = string.Empty;
            Rb14.IsChecked = false;
            Rb12.IsChecked = false;
            Rb1.IsChecked = true;
            Rb15.IsChecked = false;
            Rb2.IsChecked = false;
            Rb25.IsChecked = false;
            Rb3.IsChecked = false;
            Rb35.IsChecked = false;
            Rb4.IsChecked = false;
            cmbColors2.SelectedIndex = -1;

            PolyRaioTx.Text = string.Empty;
            PolyAltTx.Text = string.Empty;
            cmbColors3.SelectedIndex = -1;
        }
        
        private string GetQueryValues(string option, List<string> specialColumns)
        {
            CreateSQL sql = new CreateSQL();

            string sqlQueryStr;

            #region CGI query
            if (!string.IsNullOrEmpty(CgiTx.Text) || !string.IsNullOrEmpty(OperadoraTx.Text) ||
                !string.IsNullOrEmpty(LacTx.Text) || !string.IsNullOrEmpty(CidTx.Text))
            {
                sql.UseCGIQuery = true;

                if (!string.IsNullOrEmpty(CgiTx.Text))
                {
                    sql.CGI = CgiTx.Text.Trim();
                }
                if (!string.IsNullOrEmpty(OperadoraTx.Text))
                {
                    if (OperadoraTx.Text.Length == 1)
                    {
                        sql.Operadora = OperadoraTx.Text.Trim();
                    }
                    else
                    {
                        string ope = OperadoraTx.Text.Trim();
                        sql.Operadora = ope.Substring(1);
                    }
                    
                }
                if (!string.IsNullOrEmpty(LacTx.Text))
                {
                    sql.LAC = LacTx.Text.Trim();
                }
                if (!string.IsNullOrEmpty(CidTx.Text))
                {
                    sql.CID = CidTx.Text.Trim();
                }
            }
            #endregion

            #region technology query
            string tech = "";
            if (GsmCb.IsChecked.Value)
            {
                tech += "2";
            }
            if (UmtsCb.IsChecked.Value)
            {
                tech += "3";
            }
            if (LteCb.IsChecked.Value)
            {
                tech += "4";
            }
            
            if (!string.IsNullOrEmpty(tech))
            {
                sql.UseTechQuery = true;
                sql.Technology = tech.Trim();
            }
            #endregion

            #region date query
            //string data;  ainda não sei fazer
            #endregion

            #region Administrative Region Query
            if (!string.IsNullOrEmpty(CpTb.Text) || !string.IsNullOrEmpty(LocalTb.Text) ||
                !string.IsNullOrEmpty(ConcelhoTx.Text) || !string.IsNullOrEmpty(DistritoTx.Text))
            {
                sql.UseLocAdminQuery = true;

                if (!string.IsNullOrEmpty(CpTb.Text))
                {
                    sql.CodPostal = CpTb.Text.Trim();
                }
                if (!string.IsNullOrEmpty(LocalTb.Text))
                {
                    sql.Localidade = LocalTb.Text.Trim();
                }
                if (!string.IsNullOrEmpty(ConcelhoTx.Text))
                {
                    sql.Concelho = ConcelhoTx.Text.Trim();
                }
                if (!string.IsNullOrEmpty(DistritoTx.Text))
                {
                    sql.Distrito = DistritoTx.Text.Trim();
                }
            }
            #endregion

            #region Circular Fisical Region
            if(!string.IsNullOrEmpty(CirLatTx.Text) && !string.IsNullOrEmpty(CirLonTx.Text) && !string.IsNullOrEmpty(CirRaioTx.Text))
            {
                sql.UseLocFisCirc = true;
                sql.CircLat = Coordinate.coordinate(CirLatTx.Text.Trim());
                sql.CirLon = Coordinate.coordinate(CirLonTx.Text.Trim());
                sql.CirRaio = CirRaioTx.Text.Trim();
            }
            #endregion

            #region Squared Fisical Region
            if (!string.IsNullOrEmpty(QuadLat1Tx.Text) && !string.IsNullOrEmpty(QuadLon1Tx.Text) &&
                !string.IsNullOrEmpty(QuadLat2Tx.Text) && !string.IsNullOrEmpty(QuadLon2Tx.Text))
            {
                sql.UseLocFisQuad = true;
                sql.QuadLat1 = Coordinate.coordinate(QuadLat1Tx.Text.Trim());
                sql.QuadLon1 = Coordinate.coordinate(QuadLon1Tx.Text.Trim());
                sql.QuadLat2 = Coordinate.coordinate(QuadLat2Tx.Text.Trim());
                sql.QuadLon2 = Coordinate.coordinate(QuadLon2Tx.Text.Trim());
            }
            #endregion

            #region Circular Fisical Region From a CGI
            if (!string.IsNullOrEmpty(CirRaioTx.Text) && !string.IsNullOrEmpty(CgiTx.Text))
            {
                sql.UseCGIcircle = true;
                sql.CirRaio = CirRaioTx.Text.Trim();
                sql.CGI = CgiTx.Text.Trim();
            }
            #endregion

            sql.SetSqlQueryType(option, specialColumns);
            sqlQueryStr = sql.GetSqlStringQuery();
            //MessageBox.Show(sqlQueryStr); // DEBUG ##########################
            return sqlQueryStr;
        }

        private void ExpXls_Click(object sender, RoutedEventArgs e)
        {
            #region Titles string[]
            // criar excel com os títulos Latitude, Longitude, Name, description, AppendDataColumnsToDescription, Morada, Localidade, Concelho, Distrito, Código Postal, Tecnlogia, Código da Estação, Azimute
            List<string> titles = new List<string> { "Latitude", "Longitude", "Name", "Description", "AppendDataColumnsToDescription", "Morada", "Localidade", "Concelho", "Distrito", "Código Postal", "Tecnologia", "Código da Estação", "Azimute", "Azimute 2", "Data de Incerção na BD" };
            List<string> simpleTitles = new List<string> { "Latitude", "Longitude", "CGI", "Local", "Morada", "Localidade", "Concelho", "Distrito", "Código Postal", "Tecnologia", "Código da Estação", "Azimute", "Azimute 2", "Data de Incerção na BD" };

            List<string> specialColumns = new List<string>();
            string option = string.Empty;
            
            bool iconOrPoly = false;
            bool drawCircle = false;
            
            // Verificar se o utilizador preencheu Icons e Poligonos e dar ERRO
            /*if ((CircularIconsCbb2.SelectedItem != null || PushpinIconsCbb2.SelectedItem != null ||
                ShapesIconsCbb2.SelectedItem != null && cmbColors2.SelectedItem != null) &&
                (PolyRaioTx.Text != null || PushpinIconsCbb2.SelectedItem != null ||
                ShapesIconsCbb2.SelectedItem != null && cmbColors2.SelectedItem != null))
            {
                MessageBox.Show("Tem de escolher Icons ou Poligonos, não os dois ao mesmo tempo!!");
            } */
            // fazer de Excel de Icons
            if (!string.IsNullOrEmpty(IconNumTx.Text))
            {
                iconOrPoly = true;
                if (!string.IsNullOrEmpty(CirRaioTx.Text))
                {
                    drawCircle = true;
                }                
                option = "icon";
                titles.Add("Icon");
                titles.Add("IconColor");
                titles.Add("IconScale");
                titles.Add("IconHeading");

                // icon number -------------------------------------------------------------------------
                specialColumns.Add(IconNumTx.Text.Trim());

                // icon color ----------------------------------------------------------------------------
                string color = cmbColors2.SelectedItem.ToString().Replace("System.Windows.Media.Color ","");
                specialColumns.Add(color);

                // icon size ----------------------------------------------------------------------------
                string size = "1.0";
                if (Convert.ToBoolean(Rb14.IsChecked)) { size = "0.25"; }
                if (Convert.ToBoolean(Rb12.IsChecked)) { size = "0.50"; }
                if (Convert.ToBoolean(Rb15.IsChecked)) { size = "1.5"; }
                if (Convert.ToBoolean(Rb2.IsChecked)) { size = "2.0"; }
                if (Convert.ToBoolean(Rb25.IsChecked)) { size = "2.5"; }
                if (Convert.ToBoolean(Rb3.IsChecked)) { size = "3.0"; }
                if (Convert.ToBoolean(Rb35.IsChecked)) { size = "3.5"; }
                if (Convert.ToBoolean(Rb4.IsChecked)) { size = "4.0"; }
                specialColumns.Add(size);

                
            }
            // Fazer Excel de Poligonos
            else if (!string.IsNullOrEmpty(PolyRaioTx.Text) && !string.IsNullOrEmpty(PolyAltTx.Text) &&
                cmbColors3.SelectedItem != null)
            {
                iconOrPoly = true;
                if (!string.IsNullOrEmpty(CirRaioTx.Text)) { drawCircle = true; }
                titles.Add("Polygon");
                titles.Add("PolygonColor");
                titles.Add("PolygonAltitude");
                titles.Add("PolygonAmplitude");
                titles.Add("PolygonAzimute");
                option = "polygon";
                specialColumns.Add("120");
                specialColumns.Add(cmbColors3.SelectedItem.ToString().Replace("System.Windows.Media.Color ", ""));
                specialColumns.Add(PolyAltTx.Text);
                specialColumns.Add(PolyRaioTx.Text);
            }
            else
            {
                // Fazer Excel só com resultados da Base de Dados
                iconOrPoly = false;
                option = "normal";
                
            }
            #endregion

            #region DB Results
            try
            {
                // Reset the DataSet
                ds.Reset();

                // Filling Dataset with results from NpgsqlDataAdapter
                string sql = GetQueryValues(option, specialColumns);
                DaoDB.GetQueryResults(sql).Fill(ds);

                // since it c# DataSet can handle multiple tables, we will select first ?????????????
                dt = ds.Tables[0];                

                // connect grid to DataTable
                //QueryResultsData.ItemsSource = dt.DefaultView;

                // close the connection
                DaoDB.CloseDbConnection();

                // Clean Values for the next Query
                //CleanQueryValues(); só depois de escrever no Excel
            }
            catch (Exception msg)
            {
                // something went wrong, and you wanna know why
                MessageBox.Show(msg.ToString());
                throw;
            }
            #endregion

            #region Excel
            // Reset col and row
            DaoWriteXLS.Reset();
            // Set DataTable in the DaoWriteXLS
            DaoWriteXLS.dt = dt;

            // Create an excel package
            ExcelPackage p = new ExcelPackage();

            //TODO: A partir daqui tenho de arranjar uma maneira de: se for pesquisa por circulo fazer mais uma folha excel
            // que tenha o ponto central do circulo como ícone, bem como o circulo desenhado como polígono

            // Create an Excel Worksheet
            ExcelWorksheet ws = DaoWriteXLS.CreateSheet(p, "Células", 1);

            // Add Titles Row
            if (iconOrPoly) { DaoWriteXLS.AddTitlesRow(ws, titles); }
            else { DaoWriteXLS.AddTitlesRow(ws, simpleTitles); }
            
            // Add Data
            DaoWriteXLS.AddData(ws);

            // se for icon fazer mais uma folha com o ponto e o circulo
            if(drawCircle)
            {
                if (!CirRaioTx.Text.Contains(".") || !CirRaioTx.Text.Contains(","))
                {
                    CirRaioTx.Text += ".00";
                }

                float raio;
                try { raio = float.Parse(CirRaioTx.Text); }
                catch (FormatException fe) { MessageBox.Show(fe.ToString()); throw; }
            
                float raioKms = raio * 1000;

                string lat = string.Empty;
                string lon = string.Empty;
                if (!string.IsNullOrEmpty(CirLatTx.Text) && !string.IsNullOrEmpty(CirLonTx.Text))
                {
                    lat = Coordinate.coordinate(CirLatTx.Text);
                    lon = Coordinate.coordinate(CirLonTx.Text);
                }
                else if (!string.IsNullOrEmpty(CgiTx.Text))
                {
                    lat = (string)dt.Rows[0][0];
                    lon = (string)dt.Rows[0][1];
                }
                

                var circleTitles = new List<string> { "Latitude", "Longitude", "Name", "Description", "AppendDataColumnsToDescription", "Icon", "Iconcolor", "IconHeading", "IconScale", "Polygon", "PolygonColor", "PolygonAzimute", "PolygonAltitude", "PolygonAmplitude" };
                var row0 = new List<string> { lat, lon, "Ponto Central", string.Empty, string.Empty, "472", "red", "360", "1", string.Empty, string.Empty, string.Empty, string.Empty, string.Empty};
                var row1 = new List<string> { lat, lon, string.Empty, "Raio de " + CirRaioTx.Text + "Kms", string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, raioKms.ToString(), "orange", "0", "100", "360"};
                List<List<string>> circleData = new List<List<string>> { circleTitles, row0, row1 };


                if (!string.IsNullOrEmpty(CirRaioTx.Text))
                {
                    ExcelWorksheet ws2 = DaoWriteXLS.CreateSheet(p, "Ponto e Círculo", 2);

                    DaoWriteXLS.AddData(circleData, ws2);
                }               

            }
            CleanQueryValues();

            // Save File
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "Ficheiros Excel (*.xlsx)|*.xlsx|Todos os ficheiros|*.*";

            string fileName = string.Empty;
            if (sfd.ShowDialog() == true)
            {
                fileName = sfd.FileName;
            }
            try
            {
                DaoWriteXLS.SaveFile(fileName, p);
                LogTb.Text += "\n" + DateTime.Now.TimeOfDay.ToString() + " - Ficheiro '" + fileName + "' gravado com sucesso!";
            }
            catch (Exception msg)
            {
                LogTb.Text += "\n" + DateTime.Now.TimeOfDay.ToString() + " - ERRO!!! - Ficheiro não gravado!";
                LogTb.Text += "\n" + msg.ToString();
                
            }
            ResetSearchDbFrameFields();
            #endregion
        }

        private void SearchBtn_Click(object sender, RoutedEventArgs e)
        {
            UpdateDBGrid.Visibility = System.Windows.Visibility.Hidden;
            ChangeSearchFrameVisibility(true);
            AboutGrid.Visibility = System.Windows.Visibility.Hidden;
        }

        private void xls_XLS2KMZ_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "Ficheiros Excel (*.xls, *.xlsx)|*.xls; *.xlsx|Todos os ficheiros|*.*";

            if (ofd.ShowDialog() == true)
            {
                excel2kmzFilePath = ofd.FileName.ToString();
                //string safeFilePath = ofd.SafeFileName;
                //ChangeKmzPropertiesFrameVisibility(true);
                try
                {
                    if (excel2kmzFilePath.Contains(".xlsx"))
                    {
                        excel2kmzList = DaoReadXLS.readExcel(excel2kmzFilePath);
                        bool fileIsGood = FirstRowChecker.check(excel2kmzList[0][1]);
                        if (fileIsGood)
                        {
                            XLS2KMZtext.Text = " Ficheiro carregado com sucesso!";
                        }
                        else
                        {
                            XLS2KMZtext.Text = " O ficheiro escolhido não tem como primeiras células Latitude, Longitude, Name , Description!!!";
                            excel2kmzFilePath = string.Empty;
                            excel2kmzList.Clear();
                        }
                    }
                }
                catch (Exception)
                {
                    XLS2KMZtext.Text = " O ficheiro escolhido não tem extensão .xlsx! Impossível continuar!";
                    throw;
                }                                              
            }
        }

        private void Convert2kmz_Click(object sender, RoutedEventArgs e)
        {
            var xls2kmz = new Excel2KMZ();
            xls2kmz.excel2KMZ(excel2kmzFilePath, excel2kmzList);
        }
        #endregion

        #region MenuBarButtons - change frames visibility
        private void CellMapBtn_Click(object sender, RoutedEventArgs e)
        {
            CellMapGrid.Visibility = System.Windows.Visibility.Visible;
            DBGrid.Visibility = System.Windows.Visibility.Hidden;
            Xls2KmzGrid.Visibility = System.Windows.Visibility.Hidden;
            ManualGrid.Visibility = System.Windows.Visibility.Hidden;
            WelcomeGrid.Visibility = System.Windows.Visibility.Hidden;
            UpdateDBGrid.Visibility = System.Windows.Visibility.Hidden;
            ChangeSearchFrameVisibility(false);
            AboutGrid.Visibility = System.Windows.Visibility.Hidden;
            ChangeKmzPropertiesFrameVisibility(false);
            ChangeKmzSaveFrameVisibility(false);
            QueryResultsData.Visibility = System.Windows.Visibility.Hidden;
        }

        private void DbBtn_Click(object sender, RoutedEventArgs e)
        {
            CellMapGrid.Visibility = System.Windows.Visibility.Hidden;
            DBGrid.Visibility = System.Windows.Visibility.Visible;
            Xls2KmzGrid.Visibility = System.Windows.Visibility.Hidden;
            ManualGrid.Visibility = System.Windows.Visibility.Hidden;
            WelcomeGrid.Visibility = System.Windows.Visibility.Hidden;
            UpdateDBGrid.Visibility = System.Windows.Visibility.Hidden;
            ChangeSearchFrameVisibility(false);
            AboutGrid.Visibility = System.Windows.Visibility.Hidden;
            ChangeKmzPropertiesFrameVisibility(false);
            ChangeKmzSaveFrameVisibility(false);
            QueryResultsData.Visibility = System.Windows.Visibility.Hidden;
        }

        private void Xls2Kmz_Click(object sender, RoutedEventArgs e)
        {
            CellMapGrid.Visibility = System.Windows.Visibility.Hidden;
            DBGrid.Visibility = System.Windows.Visibility.Hidden;
            Xls2KmzGrid.Visibility = System.Windows.Visibility.Visible;
            ManualGrid.Visibility = System.Windows.Visibility.Hidden;
            WelcomeGrid.Visibility = System.Windows.Visibility.Hidden;
            UpdateDBGrid.Visibility = System.Windows.Visibility.Hidden;
            ChangeSearchFrameVisibility(false);
            AboutGrid.Visibility = System.Windows.Visibility.Hidden;
            ChangeKmzPropertiesFrameVisibility(false);
            ChangeKmzSaveFrameVisibility(false);
            QueryResultsData.Visibility = System.Windows.Visibility.Hidden;
        }

        private void ManualBtn_Click(object sender, RoutedEventArgs e)
        {
            CellMapGrid.Visibility = System.Windows.Visibility.Hidden;
            DBGrid.Visibility = System.Windows.Visibility.Hidden;
            Xls2KmzGrid.Visibility = System.Windows.Visibility.Hidden;
            ManualGrid.Visibility = System.Windows.Visibility.Visible;
            WelcomeGrid.Visibility = System.Windows.Visibility.Hidden;
            UpdateDBGrid.Visibility = System.Windows.Visibility.Hidden;
            ChangeSearchFrameVisibility(false);
            AboutGrid.Visibility = System.Windows.Visibility.Hidden;
            ChangeKmzPropertiesFrameVisibility(false);
            ChangeKmzSaveFrameVisibility(false);
            QueryResultsData.Visibility = System.Windows.Visibility.Hidden;
        }

        private void AboutBtn_Click(object sender, RoutedEventArgs e)
        {
            CellMapGrid.Visibility = System.Windows.Visibility.Hidden;
            DBGrid.Visibility = System.Windows.Visibility.Hidden;
            Xls2KmzGrid.Visibility = System.Windows.Visibility.Hidden;
            ManualGrid.Visibility = System.Windows.Visibility.Hidden;
            WelcomeGrid.Visibility = System.Windows.Visibility.Visible;
            UpdateDBGrid.Visibility = System.Windows.Visibility.Hidden;
            ChangeSearchFrameVisibility(false);
            AboutGrid.Visibility = System.Windows.Visibility.Visible;
            ChangeKmzPropertiesFrameVisibility(false);
            ChangeKmzSaveFrameVisibility(false);
            QueryResultsData.Visibility = System.Windows.Visibility.Hidden;
        }

        private void UpdateDbFrBt_Click(object sender, RoutedEventArgs e)
        {
            CellMapGrid.Visibility = System.Windows.Visibility.Hidden;
            DBGrid.Visibility = System.Windows.Visibility.Visible;
            Xls2KmzGrid.Visibility = System.Windows.Visibility.Hidden;
            ManualGrid.Visibility = System.Windows.Visibility.Hidden;
            WelcomeGrid.Visibility = System.Windows.Visibility.Hidden;
            ChangeSearchFrameVisibility(false);
            UpdateDBGrid.Visibility = System.Windows.Visibility.Visible;
            AboutGrid.Visibility = System.Windows.Visibility.Hidden;
            ChangeKmzPropertiesFrameVisibility(false);
            ChangeKmzSaveFrameVisibility(false);
            QueryResultsData.Visibility = System.Windows.Visibility.Hidden;
        }

        private void ChangeSearchFrameVisibility(bool searchFrameVisibility)
        {
            if (searchFrameVisibility)
            {
                SearchRect.Visibility = System.Windows.Visibility.Visible;
                SearchCgi.Visibility = System.Windows.Visibility.Visible;
                OperadoraLb.Visibility = System.Windows.Visibility.Visible;
                OperadoraTx.Visibility = System.Windows.Visibility.Visible;
                LacLb.Visibility = System.Windows.Visibility.Visible;
                LacTx.Visibility = System.Windows.Visibility.Visible;
                CidLb.Visibility = System.Windows.Visibility.Visible;
                CidTx.Visibility = System.Windows.Visibility.Visible;
                CgiLb.Visibility = System.Windows.Visibility.Visible;
                CgiTx.Visibility = System.Windows.Visibility.Visible;

                TecnologiaFr.Visibility = System.Windows.Visibility.Visible;
                GsmCb.Visibility = System.Windows.Visibility.Visible;
                UmtsCb.Visibility = System.Windows.Visibility.Visible;
                LteCb.Visibility = System.Windows.Visibility.Visible;

                DataFr.Visibility = System.Windows.Visibility.Visible;
                Date_1_Dp.Visibility = System.Windows.Visibility.Visible;
                Date_2_Dp.Visibility = System.Windows.Visibility.Visible;
                DeLb.Visibility = System.Windows.Visibility.Visible;
                ALb.Visibility = System.Windows.Visibility.Visible;
                CellActuaisCb.Visibility = System.Windows.Visibility.Visible;

                LocAdmFr.Visibility = System.Windows.Visibility.Visible;
                CpLb.Visibility = System.Windows.Visibility.Visible;
                CpTb.Visibility = System.Windows.Visibility.Visible;
                LocalLb.Visibility = System.Windows.Visibility.Visible;
                LocalTb.Visibility = System.Windows.Visibility.Visible;
                ConcelhoLb.Visibility = System.Windows.Visibility.Visible;
                ConcelhoTx.Visibility = System.Windows.Visibility.Visible;
                DistritoLb.Visibility = System.Windows.Visibility.Visible;
                DistritoTx.Visibility = System.Windows.Visibility.Visible;

                LocFisFr.Visibility = System.Windows.Visibility.Visible;
                CirculoLb.Visibility = System.Windows.Visibility.Visible;
                QuadradoLb.Visibility = System.Windows.Visibility.Visible;
                CirLatLb.Visibility = System.Windows.Visibility.Visible;
                CirLatTx.Visibility = System.Windows.Visibility.Visible;
                CirLonLb.Visibility = System.Windows.Visibility.Visible;
                CirLonTx.Visibility = System.Windows.Visibility.Visible;
                CirRaioLb.Visibility = System.Windows.Visibility.Visible;
                CirRaioTx.Visibility = System.Windows.Visibility.Visible;
                QuadLat1Lb.Visibility = System.Windows.Visibility.Visible;
                QuadLat1Tx.Visibility = System.Windows.Visibility.Visible;
                QuadLon1Lb.Visibility = System.Windows.Visibility.Visible;
                QuadLon1Tx.Visibility = System.Windows.Visibility.Visible;
                QuadLat2Lb.Visibility = System.Windows.Visibility.Visible;
                QuadLat2Tx.Visibility = System.Windows.Visibility.Visible;
                QuadLon2Lb.Visibility = System.Windows.Visibility.Visible;
                QuadLon2Tx.Visibility = System.Windows.Visibility.Visible;

                XlsKmzFr.Visibility = System.Windows.Visibility.Visible;
                IconLb.Visibility = System.Windows.Visibility.Visible;
                PolyLb.Visibility = System.Windows.Visibility.Visible;
                IconNumberLb.Visibility = System.Windows.Visibility.Visible;
                IconNumTx.Visibility = System.Windows.Visibility.Visible;
                TamanhoLb.Visibility = System.Windows.Visibility.Visible;
                Rb14.Visibility = System.Windows.Visibility.Visible;
                Rb12.Visibility = System.Windows.Visibility.Visible;
                Rb1.Visibility = System.Windows.Visibility.Visible;
                Rb15.Visibility = System.Windows.Visibility.Visible;
                Rb2.Visibility = System.Windows.Visibility.Visible;
                Rb25.Visibility = System.Windows.Visibility.Visible;
                Rb3.Visibility = System.Windows.Visibility.Visible;
                Rb35.Visibility = System.Windows.Visibility.Visible;
                Rb4.Visibility = System.Windows.Visibility.Visible;
                CorLb.Visibility = System.Windows.Visibility.Visible;
                cmbColors2.Visibility = System.Windows.Visibility.Visible;
                PolyRaioLb.Visibility = System.Windows.Visibility.Visible;
                PolyRaioTx.Visibility = System.Windows.Visibility.Visible;
                PolyAltLb.Visibility = System.Windows.Visibility.Visible;
                PolyAltTx.Visibility = System.Windows.Visibility.Visible;
                PolyCorLb.Visibility = System.Windows.Visibility.Visible;
                cmbColors3.Visibility = System.Windows.Visibility.Visible;

                XlsKmzSep.Visibility = System.Windows.Visibility.Visible;
                LocFisSep.Visibility = System.Windows.Visibility.Visible;

                ExpXlsIm1.Visibility = System.Windows.Visibility.Visible;
                ExpXlsIm2.Visibility = System.Windows.Visibility.Visible;
                ViewQuery1.Visibility = System.Windows.Visibility.Visible;
                ViewQuery2.Visibility = System.Windows.Visibility.Visible;
                ExpXlsLb.Visibility = System.Windows.Visibility.Visible;
                ViewQueryLb.Visibility = System.Windows.Visibility.Visible;
                ViewQueryBt.Visibility = System.Windows.Visibility.Visible;
                ExpXlsBt.Visibility = System.Windows.Visibility.Visible;

                LogTb.Visibility = System.Windows.Visibility.Visible;
            }
            else
            {
                SearchRect.Visibility = System.Windows.Visibility.Hidden;
                SearchCgi.Visibility = System.Windows.Visibility.Hidden;
                OperadoraLb.Visibility = System.Windows.Visibility.Hidden;
                OperadoraTx.Visibility = System.Windows.Visibility.Hidden;
                LacLb.Visibility = System.Windows.Visibility.Hidden;
                LacTx.Visibility = System.Windows.Visibility.Hidden;
                CidLb.Visibility = System.Windows.Visibility.Hidden;
                CidTx.Visibility = System.Windows.Visibility.Hidden;
                CgiLb.Visibility = System.Windows.Visibility.Hidden;
                CgiTx.Visibility = System.Windows.Visibility.Hidden;

                TecnologiaFr.Visibility = System.Windows.Visibility.Hidden;
                GsmCb.Visibility = System.Windows.Visibility.Hidden;
                UmtsCb.Visibility = System.Windows.Visibility.Hidden;
                LteCb.Visibility = System.Windows.Visibility.Hidden;

                DataFr.Visibility = System.Windows.Visibility.Hidden;
                Date_1_Dp.Visibility = System.Windows.Visibility.Hidden;
                Date_2_Dp.Visibility = System.Windows.Visibility.Hidden;
                DeLb.Visibility = System.Windows.Visibility.Hidden;
                ALb.Visibility = System.Windows.Visibility.Hidden;
                CellActuaisCb.Visibility = System.Windows.Visibility.Hidden;

                LocAdmFr.Visibility = System.Windows.Visibility.Hidden;
                CpLb.Visibility = System.Windows.Visibility.Hidden;
                CpTb.Visibility = System.Windows.Visibility.Hidden;
                LocalLb.Visibility = System.Windows.Visibility.Hidden;
                LocalTb.Visibility = System.Windows.Visibility.Hidden;
                ConcelhoLb.Visibility = System.Windows.Visibility.Hidden;
                ConcelhoTx.Visibility = System.Windows.Visibility.Hidden;
                DistritoLb.Visibility = System.Windows.Visibility.Hidden;
                DistritoTx.Visibility = System.Windows.Visibility.Hidden;

                LocFisFr.Visibility = System.Windows.Visibility.Hidden;
                CirculoLb.Visibility = System.Windows.Visibility.Hidden;
                QuadradoLb.Visibility = System.Windows.Visibility.Hidden;
                CirLatLb.Visibility = System.Windows.Visibility.Hidden;
                CirLatTx.Visibility = System.Windows.Visibility.Hidden;
                CirLonLb.Visibility = System.Windows.Visibility.Hidden;
                CirLonTx.Visibility = System.Windows.Visibility.Hidden;
                CirRaioLb.Visibility = System.Windows.Visibility.Hidden;
                CirRaioTx.Visibility = System.Windows.Visibility.Hidden;
                QuadLat1Lb.Visibility = System.Windows.Visibility.Hidden;
                QuadLat1Tx.Visibility = System.Windows.Visibility.Hidden;
                QuadLon1Lb.Visibility = System.Windows.Visibility.Hidden;
                QuadLon1Tx.Visibility = System.Windows.Visibility.Hidden;
                QuadLat2Lb.Visibility = System.Windows.Visibility.Hidden;
                QuadLat2Tx.Visibility = System.Windows.Visibility.Hidden;
                QuadLon2Lb.Visibility = System.Windows.Visibility.Hidden;
                QuadLon2Tx.Visibility = System.Windows.Visibility.Hidden;

                XlsKmzFr.Visibility = System.Windows.Visibility.Hidden;
                IconLb.Visibility = System.Windows.Visibility.Hidden;
                PolyLb.Visibility = System.Windows.Visibility.Hidden;
                IconNumberLb.Visibility = System.Windows.Visibility.Hidden;
                IconNumTx.Visibility = System.Windows.Visibility.Hidden;
                TamanhoLb.Visibility = System.Windows.Visibility.Hidden;
                Rb14.Visibility = System.Windows.Visibility.Hidden;
                Rb12.Visibility = System.Windows.Visibility.Hidden;
                Rb1.Visibility = System.Windows.Visibility.Hidden;
                Rb15.Visibility = System.Windows.Visibility.Hidden;
                Rb2.Visibility = System.Windows.Visibility.Hidden;
                Rb25.Visibility = System.Windows.Visibility.Hidden;
                Rb3.Visibility = System.Windows.Visibility.Hidden;
                Rb35.Visibility = System.Windows.Visibility.Hidden;
                Rb4.Visibility = System.Windows.Visibility.Hidden;
                CorLb.Visibility = System.Windows.Visibility.Hidden;
                cmbColors2.Visibility = System.Windows.Visibility.Hidden;
                PolyRaioLb.Visibility = System.Windows.Visibility.Hidden;
                PolyRaioTx.Visibility = System.Windows.Visibility.Hidden;
                PolyAltLb.Visibility = System.Windows.Visibility.Hidden;
                PolyAltTx.Visibility = System.Windows.Visibility.Hidden;
                PolyCorLb.Visibility = System.Windows.Visibility.Hidden;
                cmbColors3.Visibility = System.Windows.Visibility.Hidden;

                XlsKmzSep.Visibility = System.Windows.Visibility.Hidden;
                LocFisSep.Visibility = System.Windows.Visibility.Hidden;

                ExpXlsIm1.Visibility = System.Windows.Visibility.Hidden;
                ExpXlsIm2.Visibility = System.Windows.Visibility.Hidden;
                ViewQuery1.Visibility = System.Windows.Visibility.Hidden;
                ViewQuery2.Visibility = System.Windows.Visibility.Hidden;
                ExpXlsLb.Visibility = System.Windows.Visibility.Hidden;
                ViewQueryLb.Visibility = System.Windows.Visibility.Hidden;
                ViewQueryBt.Visibility = System.Windows.Visibility.Hidden;
                ExpXlsBt.Visibility = System.Windows.Visibility.Hidden;

                LogTb.Visibility = System.Windows.Visibility.Hidden;
            }
        }
        
        private void ChangeKmzPropertiesFrameVisibility(bool kmzFrameVisibility)
        {
            if (kmzFrameVisibility)
            {
                XLStext.Visibility = System.Windows.Visibility.Visible;
                MapMarkerImg.Visibility = System.Windows.Visibility.Visible;
                KmzLb.Visibility = System.Windows.Visibility.Visible;
                kmzProperties.Visibility = System.Windows.Visibility.Visible;
                KmzPropertiesFr.Visibility = System.Windows.Visibility.Visible;

                KmzIconLb1.Visibility = System.Windows.Visibility.Visible;
                KmzIconLb2.Visibility = System.Windows.Visibility.Visible;
                CircularIconsCbb.Visibility = System.Windows.Visibility.Visible;
                KmzIconLb3.Visibility = System.Windows.Visibility.Visible;
                PushpinIconsCbb.Visibility = System.Windows.Visibility.Visible;
                KmzIconLb4.Visibility = System.Windows.Visibility.Visible;
                ShapesIconsCbb.Visibility = System.Windows.Visibility.Visible;

                KmzTamanhoLb.Visibility = System.Windows.Visibility.Visible;
                Kmz14Rb.Visibility = System.Windows.Visibility.Visible;
                Kmz12Rb.Visibility = System.Windows.Visibility.Visible;
                Kmz1Rb.Visibility = System.Windows.Visibility.Visible;
                Kmz15Rb.Visibility = System.Windows.Visibility.Visible;
                Kmz2Rb.Visibility = System.Windows.Visibility.Visible;
                Kmz25Rb.Visibility = System.Windows.Visibility.Visible;
                Kmz3Rb.Visibility = System.Windows.Visibility.Visible;
                Kmz35Rb.Visibility = System.Windows.Visibility.Visible;
                Kmz4Rb.Visibility = System.Windows.Visibility.Visible;

                KmzCoresLb.Visibility = System.Windows.Visibility.Visible;
                cmbColors.Visibility = System.Windows.Visibility.Visible;
            }
            else
            {
                XLStext.Visibility = System.Windows.Visibility.Hidden;
                MapMarkerImg.Visibility = System.Windows.Visibility.Hidden;
                KmzLb.Visibility = System.Windows.Visibility.Hidden;
                kmzProperties.Visibility = System.Windows.Visibility.Hidden;
                KmzPropertiesFr.Visibility = System.Windows.Visibility.Hidden;

                KmzIconLb1.Visibility = System.Windows.Visibility.Hidden;
                KmzIconLb2.Visibility = System.Windows.Visibility.Hidden;
                CircularIconsCbb.Visibility = System.Windows.Visibility.Hidden;
                KmzIconLb3.Visibility = System.Windows.Visibility.Hidden;
                PushpinIconsCbb.Visibility = System.Windows.Visibility.Hidden;
                KmzIconLb4.Visibility = System.Windows.Visibility.Hidden;
                ShapesIconsCbb.Visibility = System.Windows.Visibility.Hidden;

                KmzTamanhoLb.Visibility = System.Windows.Visibility.Hidden;
                Kmz14Rb.Visibility = System.Windows.Visibility.Hidden;
                Kmz12Rb.Visibility = System.Windows.Visibility.Hidden;
                Kmz1Rb.Visibility = System.Windows.Visibility.Hidden;
                Kmz15Rb.Visibility = System.Windows.Visibility.Hidden;
                Kmz2Rb.Visibility = System.Windows.Visibility.Hidden;
                Kmz25Rb.Visibility = System.Windows.Visibility.Hidden;
                Kmz3Rb.Visibility = System.Windows.Visibility.Hidden;
                Kmz35Rb.Visibility = System.Windows.Visibility.Hidden;
                Kmz4Rb.Visibility = System.Windows.Visibility.Hidden;

                KmzCoresLb.Visibility = System.Windows.Visibility.Hidden;
                cmbColors.Visibility = System.Windows.Visibility.Hidden;
            }
        }
        
        private void ChangeKmzSaveFrameVisibility(bool saveFrameProperties)
        {
            if (saveFrameProperties)
            {
                SaveKmzImg.Visibility = System.Windows.Visibility.Visible;
                SaveKmzLb.Visibility = System.Windows.Visibility.Visible;
                SaveKMZ.Visibility = System.Windows.Visibility.Visible;
                SaveKmztext.Visibility = System.Windows.Visibility.Visible;
            }
            else
            {
                SaveKmzImg.Visibility = System.Windows.Visibility.Hidden;
                SaveKmzLb.Visibility = System.Windows.Visibility.Hidden;
                SaveKMZ.Visibility = System.Windows.Visibility.Hidden;
                SaveKmztext.Visibility = System.Windows.Visibility.Hidden;
            }
        }
        #endregion

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                    dt.Dispose();
                    ds.Dispose();
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~MainWindow() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }


        #endregion
    }
}
