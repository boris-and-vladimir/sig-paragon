﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using IronPython.Hosting;
using SharpKml.Base;
using SharpKml.Dom;
using SharpKml.Engine;
using System.Windows; // for debuging
using System.Diagnostics; // para abrir o Google Earth


namespace WpfSIGparagon
{
    class Excel2KMZ
    {
        #region Properties and Fields
        string filePath = string.Empty;
        List<List<List<String>>> excelList { get; set; }
        List<string> imageList = new List<string>();
        string photosPath = @"..\..\fotos\";  //TODO: photos path have to point to files/fotoname and then copy the foto file to a folder caled files
        string filesDirectoryPath = string.Empty;
        #endregion

        #region Methods
        public void excel2KMZ(string kmzFilePath, List<List<List<String>>> excel2kmzList)
        {
           
            filePath = kmzFilePath.Substring(0, kmzFilePath.Length - 4) + "kml";
            filesDirectoryPath = kmzFilePath.Substring(0, kmzFilePath.LastIndexOf('\\')) + "\\files";
            // criar pasta files
            System.IO.Directory.CreateDirectory(filesDirectoryPath);
            excelList = excel2kmzList;

            Kml kml = new Kml();
            
            var document = new Document();
            document.Id = "null";
            
            int startIndex = kmzFilePath.LastIndexOf('\\') + 1;
            int lenght = kmzFilePath.Length - 6 - kmzFilePath.LastIndexOf('\\');

            document.Name = kmzFilePath.Substring(startIndex, lenght);

            //kml.Feature = document;

            // Build legend if exists -----------------------------------------
            
            try
            {
                ScreenOverlay screen = new ScreenOverlay();
                screen.Name = "Legenda";
                screen.Icon = new SharpKml.Dom.Icon();
                copyFile(AppDomain.CurrentDomain.BaseDirectory + @"..\..\fotos", filesDirectoryPath, "legenda.png");
                screen.Icon.Href = new Uri(@"files/legenda.png", UriKind.Relative);
                //screen.Icon.Href = new Uri(@"file:///D:\Mega\DP\Projetos\SIG%20Paragon\WpfSIGparagon\fotos\legenda.png", UriKind.Absolute);

                OverlayVector vect = new OverlayVector();
                vect.X = 0;
                vect.Y = 1;
                vect.XUnits = Unit.Fraction;
                vect.YUnits = Unit.Fraction;
                screen.Origin = vect;

                ScreenVector screenVect = new ScreenVector();
                screenVect.X = 15;
                screenVect.Y = 0.98;
                screenVect.XUnits = Unit.Pixel;
                screenVect.YUnits = Unit.Fraction;
                screen.Screen = screenVect;

                SizeVector size = new SizeVector();
                size.X = 0;
                size.Y = 0;
                size.XUnits = Unit.Pixel;
                size.YUnits = Unit.Pixel;
                screen.Size = size;
                
                document.AddFeature(screen);
                kml.Feature = screen;
            }
            catch (Exception)
            {

                throw;
            }
            

            foreach (List<List<string>> list in excelList)
            {
                if (list.Count > 1)
                {
                    Folder folder = new Folder();
                    folder.Name = list[0][0]; // sheetname --------------------
                    document.AddFeature(folder);
                    //kml.Feature = folder;  COMENTEI ESTE
                    List<string> headers = new List<string>();// = list[1]; // column titles ----------
                    foreach (string s in list[1])
                    {
                        headers.Add(s.ToLower());
                    }
                    
                    // Optional column names to build a point -----------------
                    List<string> col_names = new List<string> {"icon",
                        "iconcolor", "iconscale", "description",
                             "appenddatacolumnstodescription", "iconheading",
                             "linestringcolor", "foto", "polygon",
                             "polygoncolor", "polygonaltitude",
                             "polygonazimute", "polygonamplitude",
                             "squarealtitude", "squarelatitude",
                             "squarelongitude", "squarecolor" };
                    // Check what optional names are in the Excel data --------
                    List<bool> col_has_name = new List<bool>();
                    
                    foreach (string name in col_names)
                    {
                        col_has_name.Add(headers.Contains(name));
                    }
                    
                    // POINT BUILD ############################################                    
                    List<string> next_coord = new List<string>();
                    for (int i = 2; i < list.Count; i++)  // Aqui estava <=
                    {
                        // Coordinate -----------------------------------------
                        List<string> coords = new List<string>
                        {
                            Coordinate.coordinate(list[i][1]),
                            Coordinate.coordinate(list[i][0])
                        };
                        if (i < list.Count - 1) // Next Coordinate (icon heading)
                        {
                            next_coord.Add(Coordinate.coordinate(list[i + 1][1]));
                            next_coord.Add(Coordinate.coordinate(list[i + 1][0]));
                        }
                        else
                        {
                            next_coord.Add(Coordinate.coordinate(list[i][1]));
                            next_coord.Add(Coordinate.coordinate(list[i][0]));
                        }
                        // Point ----------------------------------------------
                        /*string name = string.Empty;
                        try
                        {
                            name = formatedTime(list[i][2]); // TODO: arranjar aqui para só executar isso se o formato for tempo
                        }
                        catch (Exception)
                        {
                            try
                            {
                                name = (string)list[i][2];
                            }
                            catch (Exception)
                            {
                                name = list[i][2];
                            }
                        } */
                        SharpKml.Dom.Point point = new SharpKml.Dom.Point();
                        double latitude = double.Parse(list[i][0]);
                        double longitude = double.Parse(list[i][1]);
                        point.Coordinate = new SharpKml.Base.Vector(latitude, longitude);
                        Placemark placemark = new Placemark();
                        placemark.Geometry = point;
                        placemark.Name = list[i][2]; // Provisório não sei se não dará erro em datas excel
                        //placemark.Name = name;
                        var style = new SharpKml.Dom.Style();
                        style.Id = "style" + list.GetHashCode().ToString() + i.ToString();               

                        LabelStyle labelStyle = new LabelStyle();
                        // Remove the name in map if is a map of polygons or squares
                        if (col_has_name[col_names.IndexOf("polygon")] &&
                            !string.IsNullOrEmpty(list[i][headers.IndexOf("polygon")]))
                        {
                            labelStyle.Scale = 0;
                        }
                        if (col_has_name[col_names.IndexOf("squarecolor")] &&
                            !string.IsNullOrEmpty(list[i][headers.IndexOf("squarecolor")]))
                        {
                            labelStyle.Scale = 0;
                        }
                        style.Label = labelStyle;
                        // Icon -----------------------------------------------
                        IconStyle iconstyle = new IconStyle();
                        
                        if (col_has_name[col_names.IndexOf("icon")] &&
                            !string.IsNullOrEmpty(list[i][headers.IndexOf("icon")]))
                        {
                            copyFile(AppDomain.CurrentDomain.BaseDirectory + @"..\..\icons\", filesDirectoryPath, list[i][headers.IndexOf("icon")] + ".png");
                            iconstyle.Icon = new IconStyle.IconLink(new Uri(@"files/" + list[i][headers.IndexOf("icon")] + ".png", UriKind.Relative));
                            //iconstyle.Icon = new IconStyle.IconLink(new Uri(AppDomain.CurrentDomain.BaseDirectory +  @"..\..\icons\" + list[i][headers.IndexOf("icon")] + ".png", UriKind.Absolute));
                        }
                        if (col_has_name[col_names.IndexOf("polygon")] &&
                            !string.IsNullOrEmpty(list[i][headers.IndexOf("polygon")]))
                        {
                            iconstyle.Scale = 0;
                        }
                        if (col_has_name[col_names.IndexOf("squarecolor")] &&
                            !string.IsNullOrEmpty(list[i][headers.IndexOf("squarecolor")]))
                        {
                            iconstyle.Scale = 0;
                        }
                        // Icon Color -----------------------------------------
                        if (col_has_name[col_names.IndexOf("iconcolor")] &&
                            !string.IsNullOrEmpty(list[i][headers.IndexOf("iconcolor")]))
                        {
                            Color color = new Color();
                            color = Color.FromName(list[i][headers.IndexOf("iconcolor")]);
                            Color32 kmlColor = new Color32(color.A, color.B, color.G, color.R);
                            iconstyle.Color = kmlColor;
                        }
                        // Icon Scale / Size ----------------------------------
                        if (col_has_name[col_names.IndexOf("iconscale")] &&
                            !string.IsNullOrEmpty(list[i][headers.IndexOf("iconscale")]))
                        {
                            double scale;
                            if (Double.TryParse(list[i][headers.IndexOf("iconscale")], out scale))
                            {
                                iconstyle.Scale = scale;
                            }
                            else
                            {
                                MessageBox.Show(headers.IndexOf("iconscale").ToString() + " " + "Erro ao converter o número da coluna IconScale!!!\n" + list[i][headers.IndexOf("iconscale")]);
                            }                            
                        }
                        // Icon Heading / Inclination -------------------------
                        if (col_has_name[col_names.IndexOf("iconheading")] &&
                            !string.IsNullOrEmpty(list[i][headers.IndexOf("iconheading")]))
                        {
                            if ((list[i][headers.IndexOf("iconheading")]).GetType() == typeof(Double) ||
                                (list[i][headers.IndexOf("iconheading")]).GetType() == typeof(float) ||
                                (list[i][headers.IndexOf("iconheading")]).GetType() == typeof(int))
                            {
                                iconstyle.Heading = double.Parse(list[i][headers.IndexOf("iconheading")]);
                            }
                            else
                            {
                                iconstyle.Heading = iconHeading(coords, next_coord, list[i][headers.IndexOf("icon")]);
                            }
                        }
                        style.Icon = iconstyle;
                        // Point Description ----------------------------------
                        if (col_has_name[col_names.IndexOf("description")])
                        {
                            try
                            {
                                //placemark.Description.Text = formatedTime(list[i][headers.IndexOf("description")]);
                                placemark.Description = new Description() { Text = formatedTime(list[i][headers.IndexOf("description")]) };
                            }
                            catch (Exception)
                            {
                                //placemark.Description.Text = list[i][headers.IndexOf("description")];
                                placemark.Description = new Description() { Text = list[i][headers.IndexOf("description")] };
                            }
                        }
                        // More data to anexe to point description ------------
                        string add = "appenddatacolumnstodescription";
                        if (col_has_name[col_names.IndexOf(add)])
                        {
                            BalloonStyle balloon = new BalloonStyle();
                            balloon.Text = pointDescription(headers, list[i][headers.IndexOf(add)], list[i]);
                            style.Balloon = balloon;
                        }                        
                        // Color and Data to build the line -------------------
                        if (col_has_name[col_names.IndexOf("linestringcolor")] &&
                            !string.IsNullOrEmpty(list[i][headers.IndexOf("linestringcolor")]))
                        {
                            var line = new List<List<string>>();
                            line.Add(coords);
                            Color color = new Color();
                            color = Color.FromName(list[i][headers.IndexOf("linestringcolor")]);

                            if (i < list.Count) { line.Add(next_coord); }
                            else { line.Add(coords); }

                            LineString lineString = new LineString();
                            var point1 = new SharpKml.Base.Vector(double.Parse(line[0][1]), double.Parse(line[0][0]));
                            var point2 = new SharpKml.Base.Vector(double.Parse(line[1][1]), double.Parse(line[1][0]));
                            var points = new List<SharpKml.Base.Vector>();
                            points.Add(point1);
                            points.Add(point2);

                            lineString.Coordinates = new CoordinateCollection(points);
                            
                            Placemark linePlacemark = new Placemark();
                            linePlacemark.Geometry = lineString;
                            
                            SharpKml.Dom.Style lineStyle = new SharpKml.Dom.Style();
                            //Color32 color1 = new Color32(color.A, color.B, color.G, color.A);
                            //lineStyle.Line.Color = color1;
                            lineStyle.Line.Color = new Color32(color.A, color.B, color.G, color.R);
                            lineStyle.Line.Width = 2.0; // pixels


                            linePlacemark.AddStyle(lineStyle);
                            folder.AddFeature(linePlacemark);

                            line.Clear();
                        }
                        // Polygon --------------------------------------------
                        if (col_has_name[col_names.IndexOf("polygon")] &&
                            !string.IsNullOrEmpty(list[i][headers.IndexOf("polygon")]))
                        {
                            string description = list[i][headers.IndexOf("description")].ToString();
                            Polygon polygon = new Polygon();
                            polygon.AltitudeMode = AltitudeMode.RelativeToGround;
                            double radius = double.Parse(list[i][headers.IndexOf("polygon")]);
                            double altitude = double.Parse(list[i][headers.IndexOf("polygonaltitude")]);
                            double azimuth = double.Parse(list[i][headers.IndexOf("polygonazimute")]);
                            double amplitude = 60.0;
                            if (col_has_name[col_names.IndexOf("polygonamplitude")])
                            {
                                amplitude = double.Parse(list[i][headers.IndexOf("polygonamplitude")]);
                            }
                            LinearRing polPoints = new LinearRing();
                            List<SharpKml.Base.Vector> polPointsVectors = makePolygon(coords[0], coords[1], azimuth, radius, altitude, amplitude);
                            //foreach (SharpKml.Base.Vector v in polPointsVectors) { polPoints.Coordinates = new CoordinateCollection(polPointsVectors) }     //{ polPoints.Coordinates.Add(v); }
                            polPoints.Coordinates = new CoordinateCollection(polPointsVectors);
                            //polygon.OuterBoundary.LinearRing = new LinearRing();
                            //polygon.OuterBoundary.LinearRing.Coordinates = new CoordinateCollection(polPointsVectors);
                            //polygon.OuterBoundary.LinearRing = polPoints;
                            // AQUI FALTA DAR-LHE AS COORDENADAS <################################################
                            //LinearRing sqrPoints = new LinearRing();
                            /* 
                            sqrPoints.Coordinates.Add(new Vector(double.Parse(coords[0]),
                                double.Parse(coords[1]), altitude));
                            sqrPoints.Coordinates.Add(new Vector(sqrLon,
                                double.Parse(coords[1]), altitude));
                            sqrPoints.Coordinates.Add(new Vector(sqrLon, sqrLat, altitude));
                            sqrPoints.Coordinates.Add(new Vector(double.Parse(coords[0]),
                                sqrLat, altitude));
                            sqrPoints.Coordinates.Add(new Vector(double.Parse(coords[0]),
                                double.Parse(coords[1]), altitude)); */
                            //polygon.OuterBoundary.LinearRing = sqrPoints;

                            PolygonStyle polygonStyle = new PolygonStyle();
                            Color color = new Color();
                            color = Color.FromName(list[i][headers.IndexOf("polygoncolor")]);
                            polygonStyle.Color = new Color32(color.A, color.B, color.G, color.R);
                            polygonStyle.Fill = true;
                            polygonStyle.Outline = true;

                            // Criar placemark, dar-lhe nome, type polygon, etc
                            Placemark polPlacemark = new Placemark();
                            polPlacemark.Name = description;
                            polPlacemark.Geometry = polygon;

                            BalloonStyle polBallon = new BalloonStyle();
                            polBallon.Text = pointDescription(headers, list[i][headers.IndexOf(add)], list[i]);
                        }
                        // Square ---------------------------------------------
                        if (col_has_name[col_names.IndexOf("squarecolor")] &&
                                !string.IsNullOrEmpty(list[i][headers.IndexOf("squarecolor")]))
                        {
                            string description = (string)list[i][headers.IndexOf("description")];
                            Polygon polygon = new Polygon();
                            polygon.AltitudeMode = AltitudeMode.RelativeToGround;
                            folder.Name = description;
                            Color color = new Color();
                            color = Color.FromName(list[i][headers.IndexOf("squarecolor")]);
                            PolygonStyle polygonStyle = new PolygonStyle();
                            polygonStyle.Color = new Color32(color.A, color.B, color.G, color.R);
                            double altitude = double.Parse(list[i][headers.IndexOf("squarealtitude")]);
                            string sqrLatOriginal = list[i][headers.IndexOf("squarelatitude")];
                            string sqrLonOriginal = list[i][headers.IndexOf("squarelongitude")];
                            List<string> sqrCoords = new List<string>();
                            sqrCoords.Add(Coordinate.coordinate(sqrLatOriginal)); 
                            sqrCoords.Add(Coordinate.coordinate(sqrLonOriginal));
                            double sqrLat = double.Parse(sqrCoords[0]);
                            double sqrLon = double.Parse(sqrCoords[1]);
                            
                            LinearRing sqrPoints = new LinearRing();
                            sqrPoints.Coordinates.Add(new SharpKml.Base.Vector(double.Parse(coords[0]),
                                double.Parse(coords[1]), altitude));
                            sqrPoints.Coordinates.Add(new SharpKml.Base.Vector(sqrLon,
                                double.Parse(coords[1]), altitude));
                            sqrPoints.Coordinates.Add(new SharpKml.Base.Vector (sqrLon, sqrLat, altitude));
                            sqrPoints.Coordinates.Add(new SharpKml.Base.Vector(double.Parse(coords[0]),
                                sqrLat, altitude));
                            sqrPoints.Coordinates.Add(new SharpKml.Base.Vector(double.Parse(coords[0]),
                                double.Parse(coords[1]), altitude));
                            polygon.OuterBoundary.LinearRing = sqrPoints; 

                            PolygonStyle sqrStyle = new PolygonStyle();
                            Color sqrColor = new Color();
                            sqrColor = Color.FromName(list[i][headers.IndexOf("squarecolor")]);
                            polygonStyle.Color = new Color32(color.A, color.B, color.G, color.R);
                            // falta aqui qualquer coisa para mudar o alphaint para 7F
                            polygonStyle.Fill = true;
                            polygonStyle.Outline = true;

                            // Criar placemark, dar-lhe nome, type polygon, etc
                            Placemark polPlacemark = new Placemark();
                            polPlacemark.Name = description;
                            polPlacemark.Geometry = polygon;

                            BalloonStyle polBallon = new BalloonStyle();
                            polBallon.Text = pointDescription(headers, list[i][headers.IndexOf(add)], list[i]);
                        }
                        placemark.AddStyle(style);
                        folder.AddFeature(placemark);
                        //document.AddFeature(placemark);                        
                    }
                }
                kml.Feature = document;

            }
            KmlFile kmlFile = KmlFile.Create(kml, false);
            using (var stream = System.IO.File.OpenWrite(filePath))
            {
                kmlFile.Save(stream);
            }
            // TODO: Falta agarrar no .kml e na pasta files e criar um .kmz. Por fim apagar o kml e a pasta files.
            Process.Start(filePath);                
        }

        private List<SharpKml.Base.Vector> makePolygon(string latitude, string longitude, double azimuth, double radius, double altitude, double amplitude)
        {
            double lat = double.Parse(latitude);
            double lon = double.Parse(longitude);
            
            var origin = new SharpKml.Base.Vector(lat, lon, altitude);

            var roundTrianglePoints = new List<SharpKml.Base.Vector>();
            const int ellipsoid = 6378137;

            if (amplitude == 360.0)
            {
                roundTrianglePoints = sPoints(lat, lon, radius, altitude, 60, 0);
            }
            else
            {
                for(int i = 1; i <= 360; i++) // circunference degrees
                {
                    double angle = Math.PI * 2 * i / 360;
                    double dx = radius * Math.Cos(angle);
                    double dy = radius * Math.Sin(angle);
                    double pointLat = lat + (180 / Math.PI) * (dy / ellipsoid);
                    double pointLon = lon + (180 / Math.PI) * (dx / ellipsoid) / Math.Cos(lat * Math.PI / 180);
                    //SharpKml.Dom.Point point = new SharpKml.Dom.Point();
                    SharpKml.Base.Vector point = new SharpKml.Base.Vector(pointLat, pointLon, altitude);
                    int degrees = (int)Math.Round(angle * (180.0 / Math.PI));
                    var aziP1 = (azimuth + (amplitude / 2)) % 360;
                    var aziP2 = (azimuth + (amplitude / 4)) % 360;
                    var aziP3 = (azimuth - (amplitude / 4)) % 360;
                    var aziP4 = (azimuth - (amplitude / 2)) % 360;

                    if (degrees == azimuth || degrees == aziP1 || 
                        degrees == aziP2 || degrees == aziP3 || degrees == aziP4)
                    {
                        roundTrianglePoints.Add(point);
                    }

                    /* COMMENT BECAUSE .Sort doesn't work with with SharpKml Vectors
                    if ((azimuth <= (amplitude / 2)) || (azimuth >= (360 - (amplitude / 2))))
                    {
                        roundTrianglePoints.Sort();
                    }
                    */
                                            
                }
                // add original point to roundTrianglePoints
                roundTrianglePoints.Insert(0, origin);
                roundTrianglePoints.Add(origin);
            }

            return roundTrianglePoints;
        }

        private SharpKml.Base.Vector toEarth(List<double> point, double altitude)
        {
            double longitude;

            if (point[0] == 0.0) { longitude = Math.PI / 2.0; }
            else { longitude = Math.Atan(point[1] / point[0]); }
            double colatitude = Math.Acos(point[2]);
            double latitude = Math.PI / 2 - colatitude;

            // select correct branch of arctan
            if (point[0] < 0.0)
            {
                if (point[1] <= 0.0) { longitude = -(Math.PI - longitude); }
                else { longitude = Math.PI + longitude; }
            }

            const double DEG = 180 / Math.PI;

            var pointLst = new List<SharpKml.Base.Vector>();
            SharpKml.Base.Vector v = new SharpKml.Base.Vector(longitude * DEG, latitude * DEG, altitude);
            pointLst.Add(v);

            //return pointLst;
            return v;
        }

        private List<Double> toCard(double longitude, double latitude)
        {
            /*
            convert long, lat IN RADIANS to (x,y,z)

            spherical coordinate use "co-latitude", not "lattitude"
            lattiude = [-90, 90] with 0 at equator
            co-latitude = [0, 180] with 0 at north pole
            */
            double tetha = longitude;
            double phi = Math.PI / 2.0 - latitude;

            var returnLst = new List<Double>();
            returnLst.Add(Math.Cos(tetha) * Math.Sin(phi));
            returnLst.Add(Math.Sin(tetha) * Math.Sin(phi));
            returnLst.Add(Math.Cos(phi));

            return returnLst;
        }

        private List<SharpKml.Base.Vector> sPoints(double lat, double lon, double meters, double altitude, int n, double offset=0)
        {
            /*
            meters: radius of polygon
            n: number of sides
            offset: rotate polygon by number of degrees
            */
            const double RAD = Math.PI / 180.0;  // constant to convert to radians
            const double MR = 6378.1 * 1000.0; // mean radius of earth, meters
            double offsetRadians = offset * RAD;

            // compute longitude degress (in radians) at given latitude
            double r = (meters / (MR * Math.Cos(lat * RAD)));

            var vec = toCard(lon * RAD, lat * RAD);
            var pt = toCard(lon * RAD + r, lat * RAD);
            var pts = new List<SharpKml.Base.Vector>();

            for (int i = 0; i < n; i++)
            {
                pts.Add(toEarth(rotPoint(vec, pt, offsetRadians + (2.0 * Math.PI / n) * i), altitude));
            }

            pts.Add(pts[0]);

            return pts;
        }

        private List<double> rotPoint(List<double> vec, List<double> pt, double phi)
        {
            /*
            rotate point pt, around unit vector vec by phi radians
            http://blog.modp.com/2007/09/rotating-point-around-vector.html
            */

            // remap vector for sanity
            var u = vec[0];
            var v = vec[1];
            var w = vec[2];
            var x = pt[0];
            var y = pt[1];
            var z = pt[2];

            double a = u * x + v * y + w * z;
            double d = Math.Cos(phi);
            double e = Math.Sin(phi);

            var returnLst = new List<double>();

            returnLst.Add(a * u + (x - a * u) * d + (v * z - w * y) * e);
            returnLst.Add(a * v + (y - a * v) * d + (w * x - u * z) * e);
            returnLst.Add(a * w + (z - a * w) * d + (u * y - v * x) * e);

            return returnLst;
        }

        private void saveKmz(Kml kml)
        {
            throw new NotImplementedException();
            
        }

        private string pointDescription(List<string> headers, string v1, List<string> allData)
        {
            List<string> headersCopy = new List<string>(); // to manipulate a headers copy
            foreach (string s in headers)
            {
                headersCopy.Add(s.ToLower());
            }
            List<string> allDataCopy = new List<string>();//new List<string>(allData); // to amnipulate a allData copy
            foreach(string s in allData)
            {
                allDataCopy.Add(s.ToLower());
            }

            // the column "AppendDataColumnsToDescription" items
            List<string> items = new List<string>(v1.Split(','));
            List<string> fItems = new List<string>();
            foreach (string word in items)
            {
                fItems.Add(word.Trim().ToLower());
            }

            // Remove latitude and longitude and built a new Coordenadas
            if (fItems.Contains("latitude") && fItems.Contains("longitude"))
            {
                string coordenate = allDataCopy[headersCopy.IndexOf("latitude")].ToString()
                    + ", " + allDataCopy[headersCopy.IndexOf("longitude")].ToString();

                int dataIndex = Math.Min(headersCopy.IndexOf("latitude"), headersCopy.IndexOf("longitude"));
                int fItemsIndex = Math.Min(fItems.IndexOf("latitude"), fItems.IndexOf("longitude"));

                int latIndex = headersCopy.IndexOf("latitude");
                int lonIndex = headersCopy.IndexOf("longitude");
                allDataCopy.Remove(allDataCopy[latIndex]);
                allDataCopy.Remove(allDataCopy[lonIndex]);

                headersCopy.Remove("latitude");
                headersCopy.Remove("longitude");
                fItems.Remove("latitude");
                fItems.Remove("longitude");

                allDataCopy.Insert(dataIndex, coordenate);
                headersCopy.Insert(dataIndex, "coordenadas");
                fItems.Insert(fItemsIndex, "coordenadas");                
            }

            // The data indexes of the elements in AppendDataColumnsToDescription
            List<int> indexes = new List<int>();
            indexes.Add(headersCopy.IndexOf("description"));
            foreach (string word in fItems)
            {
                if (headersCopy.Contains(word))
                {
                    indexes.Add(headersCopy.IndexOf(word));
                }
            }

            // Format Dates and Times 
            if (fItems.Contains("data"))
            {
                float number1 = 0;
                int i = headersCopy.IndexOf("data");
                bool canConvert = float.TryParse(allDataCopy[i], out number1);                
                if (canConvert)
                {
                    string dateTime = allDataCopy[i];
                    allDataCopy.Remove(number1.ToString());
                    allDataCopy.Insert(i, formatedTime(dateTime));
                }
                //else
                //{
                    //allDataCopy.Insert(i, allDataCopy[i]);
                //}                
            }

            if (fItems.Contains("hora"))
            {
                float number1 = 0;
                int i = headersCopy.IndexOf("hora");
                bool canConvert = float.TryParse(allDataCopy[i], out number1);
                if (canConvert)
                {
                    string dateTime = allDataCopy[i];
                    allDataCopy.Remove(number1.ToString());
                    allDataCopy.Insert(i, formatedTime(dateTime));
                    //allDataCopy.Insert(i, formatedTime(allDataCopy[i]));
                }
                //else
                //{
                    //allDataCopy.Insert(i, name);
                //}
            }



            if (fItems.Contains("name") && allDataCopy[headersCopy.IndexOf("name")].Contains('('))
            {
                int i = headersCopy.IndexOf("name");
                string name = allDataCopy[i];
                allDataCopy.Insert(i, formatedTime(name));

            }

            // Capitalize titles
            List<string> pt = new List<string>();
            foreach(string word in fItems)
            {
                pt.Add(System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(word));
            }

            // Photos
            if (fItems.Contains("foto"))
            {
                return pointDescriptionFoto(allDataCopy, indexes, pt);
            }

            // HTML Format
            Dictionary<int, string> tags = new Dictionary<int, string>()
            {
                {0, "<BalloonStyle><text>"},
                {1, "<table><tr><td colspan=\"2\" align=\"center\">"},
                {2, "</td></tr>"},
                {3, "<tr style=\"background-color:lightgreen\"><td align=\"left\">"},
                {4, "<tr><td>"},
                {5, "</td></tr>"},
                {6, "<td>"},
                {7, "</td>"},
                {8, "</table>"},
                {9, "</text></BalloonStyle>"}
            };

            // Return builder
            string title;
            try
            {
                title = formatedTime(allDataCopy[indexes[0]]);
            }
            catch
            {
                title = allDataCopy[indexes[0]].ToString();
            }

            string head = tags[0] + tags[1] + title + tags[2];
            List<string> body = new List<string>();
            string tail = tags[8] + tags[9];

            for (int i = 1; i < indexes.Count; i++)
            {
                if (i % 2 != 0)
                {
                    body.Add(tags[3] + pt[i - 1] + ": " + tags[7] + tags[6] +
                        allDataCopy[indexes[i]].ToString() + tags[5]);
                }
                else
                {
                    body.Add(tags[4] + pt[i - 1] + ": " + tags[7] + tags[6] +
                        allDataCopy[indexes[i]].ToString() + tags[5]);
                }
            }
            string bodyStr = string.Join<string>("", body);

            // Return
            return head + bodyStr + tail;
        }

        private string pointDescriptionFoto(List<string> allData, List<int> indexes, List<string> titles)
        {
            string path = photosPath;
            string originalPath = System.IO.Directory.GetCurrentDirectory();

            // HTML Format
            Dictionary<int, string> tags = new Dictionary<int, string>()
            {
                {0, "<![CDATA[<BalloonStyle><text>"},
                {1, "<table width=\"400\" border=\"0\" cellspacing=\"5\" cellpadding=\"3\"><tr><td colspan=\"2\" align=\"center\"><font color=\"0000ff\"><b>"},
                {2, "</b></font></td></tr>"},
                {3, "<tr style=\"background-color:lightgreen\"><td align=\"center\">"},
                {4, "<tr><td colspan=\"2\" align=\"center\"></h3>"},
                {5, "</h3></td></tr>"},
                {6, "<tr><td colspan=\"2\" align=\"center\">"},
                {7, "</td></tr>"},
                {8, "</table>"},
                {9, "\n<img src=\""},
                {10, "\" alt=\"foto\" width=\"400\" height=\"280\">\n</br>"},
                {11, "<tr><td><hr></td></tr><tr style=\"backgound-color:lightgreen\"><td></td></tr>"},
                {12, "</text></BalloonStyle>]]>"}
            };

            // Build
            string head = tags[0] + tags[1] + allData[indexes[0]].ToString() + tags[2] + tags[6] + tags[7];
            var bodyLst = new List<string>();
            string tail = tags[8] + tags[12];

            for (int i = 0; i < titles.Count; i++)
            {
                if (titles[i].ToLower().Contains("foto"))
                {
                    if (!System.IO.File.Exists(AppDomain.CurrentDomain.BaseDirectory + photosPath + allData[indexes[i+1]]))
                    {
                        System.Windows.MessageBox.Show("O nome da foto no Excel difere do nome da foto na pasta 'fotos'!!!");
                    }
                    else
                    {
                        copyFile(AppDomain.CurrentDomain.BaseDirectory + photosPath, filesDirectoryPath, allData[indexes[i + 1]]);
                        bodyLst.Add(tags[6] + tags[9] + "files/" + allData[indexes[i + 1]] + tags[10] + tags[7]);
                        if (!imageList.Contains(path + allData[indexes[i + 1]]))
                        {
                            imageList.Add(path + allData[indexes[i + 1]]);
                        }
                    }
                }
                else if (titles[i].Contains("Descricao") || titles[i].Contains("Descrição"))
                {
                    bodyLst.Add(tags[6] + allData[indexes[i + 1]] + tags[7] + tags[11]);
                }
                else
                {
                    bodyLst.Add(tags[3] + allData[indexes[i + 1]] + tags[7]);
                }
            }
            string body = string.Join<string>("", bodyLst);

            // Return
            return head + body + tail;
        }

        private double iconHeading(List<string> coords, List<string> next_coords, string icon)
        {
            /*
            In a right angled triangle, the hypotenuse is is the side opposite 
            to the 90 degrees angle, the opposite is the side opposite to the
            angle we want to find, and, the adjacent, is the side who joins the 
            angle we want to find to the 90 degree angle.
            To find that angle we need to use the Inverse Tangent or ArcTangent

            The Tangent of the angle ø is:
                tan(ø) = Opposite / Adjacent
            So, the inverse Tangent is:
                tan^-1(Opposite / Adjacent) = ø

            See: http://www.mathsisfun.com/algebra/trig-inverse-sin-cos-tan.html
            */

            double adjac = double.Parse(next_coords[1]) - double.Parse(coords[1]);
            double oppos = double.Parse(next_coords[0]) - double.Parse(coords[0]);
            double angle = 0.0;  // If first adjac is 0

            if (adjac != 0.0) // avoid ZeroDivisionError
            {
                angle = Math.Atan(oppos / adjac);
            }
            else
            {
                if (oppos < 0) { return 90.0; }
                else { return -90.0; }
            }

            if (icon == "38" || icon == "106" || icon == "338" || icon == "406")
            {
                if (adjac < 0) { return angle * (180.0 / Math.PI); } // angle in degrees diference of negative longitude
                else { return angle * (180.0 / Math.PI) - 180; } // angle in degrees diference of positive longitude
            }

            return 0.0; // other icons
        }

        private string formatedTime(string ExcelTime)
        {
            DateTime dateTime = DateTime.FromOADate(double.Parse(ExcelTime));
            return dateTime.ToString();
        }

        private void copyFile(string sourcePath, string destinationPath, string fileName)
        {
            string sourceFile = System.IO.Path.Combine(sourcePath, fileName);
            string destinationFile = System.IO.Path.Combine(destinationPath, fileName);

            if (!System.IO.File.Exists(destinationPath))
            {
                System.IO.File.Copy(sourceFile, destinationFile, true);
            }
        }
        #endregion
    }
}
