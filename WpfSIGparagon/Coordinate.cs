﻿using System;
using System.Linq;

namespace WpfSIGparagon
{
    class Coordinate
    {
        #region Properties and Fields
        private static string LatOrLon { get; set; }
        #endregion

        #region Methods
        #region public method
        public static string coordinate(string lat_or_lon)
        {
            LatOrLon = lat_or_lon;

            // tirar espaços em branco
            RemoveSpaces();

            // remover caracteres
            RemoveChars();

            // ver o formato
            string format = GetFormat();

            // converter
            if (format == "dms")
            {
                ConvertDMS();
            }
            else if (format == "h")
            {
                ConvertH();
            }

            // retornar
            return LatOrLon;
        }
        #endregion

        #region private / helper methods
        private static void RemoveSpaces()
        {
            LatOrLon.Replace(" ", "");
        }

        private static void RemoveChars()
        {
            string temp = "";
            char[] pos_chars = { 'N', 'E', 'n', 'e' };
            char[] neg_chars = { 'S', 'W', 'O', 's', 'w', 'o' };

            for (int i = 0; i < LatOrLon.Length; i++)
            {
                if (pos_chars.Contains(LatOrLon[i]))
                {
                    temp += "";
                }
                else if (neg_chars.Contains(LatOrLon[i]))
                {
                    temp = "-" + temp;
                }
                else
                {
                    temp += LatOrLon[i];
                }
            }
            LatOrLon = temp;
        }

        private static string GetFormat()
        {
            // dms - Degrees Minutes Seconds format
            // h - Hybrid format
            // dd - Decimal Degrees format
            char m = '\''; // minutes
            string s = "''"; // seconds
            char s2 = '"'; // seconds

            if (LatOrLon.Contains(s) || LatOrLon.Contains(s2))
            {
                return "dms";
            }
            else if (LatOrLon.Contains(m) && !LatOrLon.Contains(s))
            {
                return "h";
            }
            else
            {
                return "dd";
            }
        }

        private static void ConvertDMS()
        {
            // Converts from Degreees Minutes Seconds format to Decimal Degrees format
            // Decimal Degrees = Degrees + Minutes / 60 + Seconds / 3600
            string coordinate = LatOrLon.Replace("\"", "''");
            string sign = "";
            string deg = "0";
            string min = "0";
            int degIndex = 0;

            int minIndex = coordinate.IndexOf('\'');

            if (LatOrLon.Contains("º"))
            {
                degIndex = coordinate.IndexOf('º');                
               
            }
            else if (coordinate.Contains("°"))
            {
                degIndex = coordinate.IndexOf('°');                
            }

            int secIndex = coordinate.IndexOf("''");
            int indexDegreeMinDiference = minIndex - degIndex;
            int indexMinSecDiference = secIndex - minIndex;

            deg = coordinate.Substring(0, degIndex);
            min = coordinate.Substring(degIndex + 1, indexDegreeMinDiference - 1);
            string sec = coordinate.Substring(minIndex + 1, indexMinSecDiference - 1); 

            int degree = Convert.ToInt32(deg);
            double minutes = Convert.ToDouble(min);
            double seconds = Convert.ToDouble(sec);

            if (degree < 0)
            {
                sign = "-";
            }

            double numericCoordinate = Math.Abs(degree) + (minutes / 60.0) + (seconds / 3600.0);

            LatOrLon = sign + numericCoordinate.ToString();
        }

        private static void ConvertH()
        {
            string coordinate = LatOrLon;
            string sign = "";
            string deg = "0";
            string min = "0";

            int degIndex = 0;
            int minIndex = coordinate.IndexOf('\'');

            if (LatOrLon.Contains("º"))
            {
                degIndex = coordinate.IndexOf('º');
            }
            else if (coordinate.Contains("°"))
            {
                degIndex = coordinate.IndexOf('°');
            }

            deg = coordinate.Substring(0, degIndex);
            int indexDegreeMinDiference = minIndex - degIndex;
            min = coordinate.Substring(degIndex + 1, indexDegreeMinDiference - 1);

            int degree = Convert.ToInt32(deg);
            double minutes = Convert.ToDouble(min);

            if (degree < 0)
            {
                sign = "-";
            }

            double numericCoordinate = Math.Abs(degree) + (minutes / 60.0);

            LatOrLon = sign + numericCoordinate.ToString();
        }
        #endregion
        #endregion
    }
}
