﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfSIGparagon
{
    class CreateSQL
    {
        #region Properties
        private string sql = string.Empty;
        private bool firstFilter = false; // to append AND after the first filter object in the WHERE clause
        private bool alreadyUsedCGI = false;
        #endregion

        #region Fields
        // CGI ----------------------------------------------------------------
        public bool UseCGIQuery { get; set; }
        public string Operadora { get; set; }
        public string LAC { get; set; }
        public string CID { get; set; }
        public string CGI { get; set; }

        // Technology
        public bool UseTechQuery { get; set; }
        public string Technology { get; set; }

        // Data
        // TODO: não sei fazer ainda  #########################################
        public bool UseDateQuery { get; set; }

        // Localização Administrativa
        public bool UseLocAdminQuery { get; set; }
        public string CodPostal { get; set; }
        public string Localidade { get; set; }
        public string Concelho { get; set; }
        public string Distrito { get; set; }

        // Localização Física - Círculo
        public bool UseLocFisCirc { get; set; }
        private string circLat;
        public string CircLat
        {
            get
            {
                return circLat;
            }

            set
            {
                circLat = Coordinate.coordinate(value);
            }
        }
        private string cirLon;
        public string CirRaio { get; set; }
        public string CirLon
        {
            get
            {
                return cirLon;
            }

            set
            {
                cirLon = Coordinate.coordinate(value);
            }
        }

        // Localização Física - CGI com Círculo
        public bool UseCGIcircle { get; set; }

        // Localização Física - Quadrado
        public bool UseLocFisQuad { get; set; }
        private string quadLat1;
        public string QuadLat1
        {
            get
            {
                return quadLat1;
            }

            set
            {
                quadLat1 = Coordinate.coordinate(value);
            }
        }
        private string quadLon1;
        public string QuadLon1
        {
            get
            {
                return quadLon1;
            }

            set
            {
                quadLon1 = Coordinate.coordinate(value);
            }
        }
        private string quadLat2;
        public string QuadLat2
        {
            get
            {
                return quadLat2;
            }

            set
            {
                quadLat2 = Coordinate.coordinate(value);
            }
        }
        private string quadLon2;
        public string QuadLon2
        {
            get
            {
                return quadLon2;
            }

            set
            {
                quadLon2 = Coordinate.coordinate(value);
            }
        }
        #endregion

        #region Methods
        public void SetSqlQueryType(string option, List<string> specialColumns)
        {
            string appendDataColumnsToDescription = "Morada, Localidade, Concelho, Distrito, Código Postal, Tecnologia, Código da Estação, Azimute, Azimute 2, Date";

            if (option == "normal")
            {
                sql = "SELECT lat, lon, cgi, sitename, address, localidade, concelho, distrito, codpostal, technology, codestation, azimuth, azimuth2, date FROM cgi ";
            }
            else if (option == "icon")  // Falta Alterar
            {
                string iconN = specialColumns[0];
                string iconColor = specialColumns[1];
                string iconScale = specialColumns[2];

                sql = "SELECT lat, lon, cgi, sitename, '" + appendDataColumnsToDescription + "' AS appendDataColumnsToDescription,  address, localidade, concelho, distrito, codpostal, technology, codestation, azimuth, azimuth2, date,'" + iconN + "' AS icon, '" + iconColor + "' AS IconColor, '" + iconScale + "' AS IconScale, cast(cast(NULLIF(azimuth, '') AS int) + 180 AS text) AS IconHeading FROM cgi \n";
            }
            else if (option == "polygon")  // Falta Alterar
            {
                string polygon = specialColumns[0];
                string polygonColor = specialColumns[1];
                string polygonAltitude = specialColumns [2];
                string polygonAmplitude = specialColumns[3];
                
                sql = "SELECT lat, lon, cgi, sitename, '" + appendDataColumnsToDescription + "' AS appendDataColumnsToDescription,  address, localidade, concelho, distrito, codpostal, technology, codestation, azimuth, azimuth2, date, '" + polygon + "' AS polygon, '" + polygonColor + "' AS polygonColor, '" + polygonAltitude + "' AS polygonAltitude, '" + polygonAmplitude + "' AS polygonAmplitude, azimuth AS polygonAzimute FROM cgi \n";
            }
        }
        
        public string GetSqlStringQuery()
        {
            // Global Query Filter 
            if (UseCGIQuery || UseTechQuery || UseLocAdminQuery || UseLocFisCirc || UseLocFisQuad || UseCGIcircle)
            {
                sql += "WHERE ";

                // Localização Física - Círculo -- TEM DE SER A PRIMEIRA POR A VARIAVÉL sql ter de ficar no meio da query
                if(UseLocFisCirc)
                {
                    LocFisCirc();                    
                }
                // Localização Física - CGI com Círculo
                if (UseCGIcircle)
                {
                    CircFromCGI();
                }
                // Localização Física - Quadrado --
                if (UseLocFisQuad)
                {
                    LocFisQuad();                    
                }
                // Cgi -----------------------------
                if (UseCGIQuery)
                {
                    CGIQuery();                    
                }
                // Technology ----------------------
                if (UseTechQuery)
                {
                    TechQuery();                    
                }
                // Localização Administrativa -------
                if (UseLocAdminQuery)
                {
                    AdminQuery();                    
                } 
            }
            sql += ";";
            return sql;           
        }

        // to add an "AND" to subsequent not first parameter query
        private void FirstFilter()
        {
            if (!firstFilter)
            {
                firstFilter = true;
            }
            else
            {
                sql += " AND ";
            }
        }
        
        // Localização Administrativa ---------------------
        private void AdminQuery()
        {
            if (!string.IsNullOrEmpty(CodPostal))
            {
                FirstFilter();
                sql += "codpostal LIKE '%" + CodPostal + "%'";
            }
            if (!string.IsNullOrEmpty(Localidade))
            {
                FirstFilter();
                sql += "sitename = '" + Localidade + "'";
            }
            if (!string.IsNullOrEmpty(Concelho))
            {
                FirstFilter();
                sql += "concelho = '" + Concelho + "'";
            }
            if (!string.IsNullOrEmpty(Distrito))
            {
                FirstFilter();
                sql += "distrito = '" + Distrito + "'";
            }
        }

        // Tecnologia ---------------------------------------
        private void TechQuery()
        {
            if (!string.IsNullOrEmpty(Technology))
            {
                if (Technology.Contains('2'))
                {
                    FirstFilter();
                    sql += "(technology = 'GSM' OR technology = '2G')";
                }
                if (Technology.Contains('3'))
                {
                    FirstFilter();
                    sql += "(technology = 'WCDMA' OR technology = 'HSDPA' OR technology = 'UMTS' OR technology = '3G')";
                }
                if (Technology.Contains('4'))
                {
                    FirstFilter();
                    sql += "(technology = 'WIMAX' OR technology LIKE '%LTE%' OR technology LIKE '%FDD%')";
                }
            }
        }
        
        // CCI (LAC, CID, Operadora, CGI) -------------------
        private void CGIQuery()
        {
            if (!string.IsNullOrEmpty(Operadora))
            {
                FirstFilter();
                sql += "operator=" + "'" + Operadora + "'";
            }
            if (!string.IsNullOrEmpty(LAC))
            {
                FirstFilter();
                sql += "lac=" + "'" + LAC + "'";
            }
            if (!string.IsNullOrEmpty(CID))
            {
                FirstFilter();
                sql += "cid=" + "'" + CID + "'";
            }
            if (!string.IsNullOrEmpty(CGI) && !alreadyUsedCGI)
            {
                FirstFilter();
                sql += "(cgi=" + "'" + CGI + "' OR cgiparagon LIKE " + "'%" + CGI + "%')";
            }
        }

        // Localização Física - Quadrado -----------------------
        private void LocFisQuad()
        {
            if (!string.IsNullOrEmpty(QuadLat1) & !string.IsNullOrEmpty(QuadLat2) & !string.IsNullOrEmpty(QuadLon1) & !string.IsNullOrEmpty(QuadLon2))
            {
                string minLat = Math.Min(decimal.Parse(QuadLat1, System.Globalization.CultureInfo.InvariantCulture.NumberFormat), decimal.Parse(QuadLat2, System.Globalization.CultureInfo.InvariantCulture.NumberFormat)).ToString();
                string maxLat = Math.Max(decimal.Parse(QuadLat1, System.Globalization.CultureInfo.InvariantCulture.NumberFormat), decimal.Parse(QuadLat2, System.Globalization.CultureInfo.InvariantCulture.NumberFormat)).ToString();
                string minLon = Math.Min(decimal.Parse(QuadLon1, System.Globalization.CultureInfo.InvariantCulture.NumberFormat), decimal.Parse(QuadLon2, System.Globalization.CultureInfo.InvariantCulture.NumberFormat)).ToString();
                string maxLon = Math.Max(decimal.Parse(QuadLon1, System.Globalization.CultureInfo.InvariantCulture.NumberFormat), decimal.Parse(QuadLon2, System.Globalization.CultureInfo.InvariantCulture.NumberFormat)).ToString();

                sql += "lat > '" + minLat + "' AND lat < '" + maxLat + "' AND lon > '" + minLon + "' AND lon < '" + maxLon + "'";
            }
        }

        // Localização Física - Círculo -------------------------
        private void LocFisCirc()
        {
            if (!string.IsNullOrEmpty(CircLat) & !string.IsNullOrEmpty(CirLon) & !string.IsNullOrEmpty(CirRaio))
            {
                float radius = float.Parse(CirRaio, System.Globalization.CultureInfo.InvariantCulture.NumberFormat) * 1000;
                string coords = CirLon + " " + CircLat;
                string sql1 = "WITH myPoint AS (SELECT ST_PointFromText('POINT(" + coords + ")', 4326) AS point) ";
                string sql2 = "ST_DWithin((SELECT point FROM myPoint), cgi.point, " + radius + " )";
                string sql3 = sql1 + sql + sql2;
                sql = sql3;
                FirstFilter();
            }
        }

        // Localização Física - CGI rodeado de Círculo -----------------------
        private void CircFromCGI()
        {
            FirstFilter();
            sql += "cgi='" + CGI + "' OR ";
            // construir busca por circulo
            float radius = float.Parse(CirRaio, System.Globalization.CultureInfo.InvariantCulture.NumberFormat) * 1000;
            string sql2 = "St_DWithin((SELECT point from cgi where cgi.cgi= '" + CGI + "'), cgi.point, " + radius + " )";
            sql += sql2;
            alreadyUsedCGI = true;
        }
        #endregion
    }
}
