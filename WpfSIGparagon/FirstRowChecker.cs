﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfSIGparagon
{
    class FirstRowChecker
    {
        #region Properties and Fields
        public static List<String> firstRow { get; set; }
        #endregion

        #region Methods
        public static bool check(List<String> firstRow)
        {
            var low = new List<string>();
            var eng = new List<string> { "latitude", "longitude", "name", "description" };
            var por = new List<string> { "latitude", "longitude", "nome", "descricao" };
            var ptr = new List<string> { "latitude", "longitude", "nome", "descrição" };

            if (firstRow != null)
            {
                for (int i = 0; i < 4; i++)
                {
                    low.Add(firstRow[i].ToLower());
                }
                
            }
            return low.SequenceEqual(eng) || low.SequenceEqual(por) || low.SequenceEqual(ptr);
        }
        #endregion
    }
}
