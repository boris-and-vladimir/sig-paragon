﻿using System;
using System.Collections.Generic;
using OfficeOpenXml; // EPPlus.dll to create Excel
using System.IO;
using System.Windows;

namespace WpfSIGparagon
{
    class DaoWriteXLS
    {
        #region Fields
        private static int colIndex = 1;
        private static int rowIndex = 1;
        #endregion

        #region Properties
        public static System.Data.DataTable dt { get; set; }
        #endregion

        #region Methods
        public static void Reset()
        {
            colIndex = 1;
            rowIndex = 1;
        }
        public static ExcelWorksheet CreateSheet (ExcelPackage p, String sheetName, int workSheetNumber)
        {
            p.Workbook.Worksheets.Add(sheetName);
            p.Workbook.Properties.Author = "Boris e Vladimir Software";
            ExcelWorksheet ws = p.Workbook.Worksheets[workSheetNumber];
            ws.Name = sheetName; //Setting Sheet's name
            ws.Cells.Style.Font.Size = 11; //Default font size for whole sheet
            ws.Cells.Style.Font.Name = "Calibri";

            return ws;
        }

        public static void AddTitlesRow (ExcelWorksheet ws, List<string> titles)
        {
            foreach (string title in titles)
            {
                var cell = ws.Cells[rowIndex, colIndex];

                //Setting the background color of header cells to Gray
                var fill = cell.Style.Fill;
                fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                fill.BackgroundColor.SetColor(System.Drawing.Color.Gray);

                cell.Value = title;

                colIndex++;
            }
            colIndex = 1;
            rowIndex++;
        }

        public static void AddData(ExcelWorksheet ws)
        {
            foreach (System.Data.DataRow dr in dt.Rows)
            {
                foreach (System.Data.DataColumn dc in dt.Columns)
                {
                    var cell = ws.Cells[rowIndex, colIndex];
                    // depois dependente do tipo de valor converter
                    // cell.Value = Convert.ToInt32(dr.[dc.ColumnName]);
                    cell.Value = dr[dc.ColumnName];

                    colIndex++;
                }
                colIndex = 1;
                rowIndex++;                
            }
        }

        public static void AddData(List<List<string>> data, ExcelWorksheet ws)
        {
            int rowNumber = 1;

            foreach (var dataRow in data)
            {
                int colNumber = 1;
                foreach (var dataCol in dataRow)
                {
                    ws.Cells[rowNumber, colNumber].Value = dataCol;
                    colNumber++;                   
                }
                rowNumber++;
            }
        }

        public static void SaveFile(String fileName, ExcelPackage p)
        {
            Byte[] bin = p.GetAsByteArray();
            System.IO.File.WriteAllBytes(fileName, bin);
        }
        #endregion
    }

    class DaoReadXLS
    {
        #region Properties and Fields
        public static System.Data.DataTable dt { get; set; }  // para fazer a lista com tudo o que vem do Excel
        
        public static String excelFileName { get; set; } // para guardar o nome do ficheiro Excel

        private List<string> data_row { get; set; } // para ter os dados das células de uma linha excel
        #endregion

        #region Methods
        public static List<List<List<string>>> readExcel(string fileName)
        {
            var fi = new FileInfo(fileName);
            //byte[] file = File.ReadAllBytes(fileName);
            //MemoryStream fi = new MemoryStream(file);
            using (ExcelPackage xlPackage = new ExcelPackage(fi))
            {
                ExcelWorkbook workbook = xlPackage.Workbook;

                var resultsList = new List<List<List<string>>>();


                if (workbook != null)
                {
                    if (workbook.Worksheets.Count > 0)
                    {
                        for (int iWorksheet = 1; iWorksheet <= workbook.Worksheets.Count; iWorksheet++)
                        {
                            var innerResultsList = new List<List<string>>();
                            ExcelWorksheet currentWorksheet = workbook.Worksheets[iWorksheet];
                            var worksheetNameList = new List<string>();
                            worksheetNameList.Add(currentWorksheet.Name);
                            int TotalRows = currentWorksheet.Dimension.End.Row;
                            int TotalCols = currentWorksheet.Dimension.End.Column;

                            innerResultsList.Add(worksheetNameList);
                            for (int iRow = 1; iRow <= TotalRows; iRow++)
                            {
                                var innerList = new List<String>();
                                for (int iCol = 1; iCol <= TotalCols; iCol++)
                                {
                                    if (currentWorksheet.Cells[iRow, iCol].Value != null)
                                    {                                        
                                        innerList.Add(currentWorksheet.Cells[iRow, iCol].Value.ToString());                                        
                                    }
                                    else
                                    {
                                        innerList.Add("");
                                    }
                                }
                                innerResultsList.Add(innerList);
                            }
                            resultsList.Add(innerResultsList);
                        }
                    }

                }
                return resultsList;

            } // the using statement calls Dispose() which closes the package.            
        }
        #endregion
    }
}
