﻿using System;
using Npgsql; // to connect to PostgreSQL
using System.Windows; // to DB notifications
namespace WpfSIGparagon
{
    class DaoDB
    {
        #region Properties
        private static NpgsqlConnection conn;
        private static bool connected = false;
        #endregion

        #region Methods
        private static void ConnectDB()
        {
            // PostgreSQL-style connection string
            string connstring = String.Format("Server={0}; Port={1};" +
                "User Id={2};Password={3};Database={4}", "127.0.0.1", "5432",
                "postgres", "dapbfgnr", "SigParagon");

            // Making connection with Npgsql provider
            conn = new NpgsqlConnection(connstring + ";ContinuousProcessing=true");
            conn.Open();
            conn.Notification += (o, e) => MessageBox.Show("Notificação do motor da Base de Dados!");
        }

        public static void CloseDbConnection()
        {
            conn.Close();
            connected = false;
        }
        
        public static NpgsqlDataAdapter GetQueryResults(string sql)
        {
            ConnectDB();
            connected = true;

            // Data adapter making request from our connection
            NpgsqlDataAdapter da = new NpgsqlDataAdapter(sql, conn);

            return da;
        }

        public static string executeCommand(string command)
        {            
            if (!connected)
            {
                ConnectDB();
                connected = true;
            }
            
            var execute = new NpgsqlCommand(command, conn);
            string result = string.Empty;
            try
            {
                result = execute.ExecuteNonQuery().ToString();
            }
            catch (Exception)
            {

                throw;
            }
            return result;
        }
        #endregion
    }
}
