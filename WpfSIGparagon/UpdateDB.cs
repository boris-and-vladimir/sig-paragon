﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Npgsql;
using NpgsqlTypes;
using System.Windows; // for debuging

namespace WpfSIGparagon
{
    class UpdateDB
    {
        #region Properties
        private static string result = string.Empty;
        private static string insertedRows = string.Empty;
        #endregion

        #region Fields
        private string createTemp = @"CREATE TEMPORARY TABLE temp(cgi text NOT NULL, 
                                                       cid text NOT NULL,
                                                       lac text NOT NULL,
                                                       operator text NOT NULL, 
                                                       technology text, 
                                                       codestation text, 
                                                       cgiparagon text, 
                                                       sitename text NOT NULL,
                                                       azimuth text NOT NULL, 
                                                       date text NOT NULL, 
                                                       azimuth2 text, 
                                                       address text, 
                                                       localidade text,
                                                       concelho text,
                                                       distrito text,
                                                       codpostal text, 
                                                       lat text, 
                                                       lon text, 
                                                       point geography(Point, 4326),
                                                       CONSTRAINT pk_cgi PRIMARY KEY (cgi, date))
                        WITH (OIDS=FALSE);
                        ALTER TABLE temp OWNER TO postgres;";

        private string updateGeoPoints = "UPDATE temp SET point = ST_GeomFromText('POINT('||lon||' '||lat||')', 4326);";

        private string insertValues = @"INSERT INTO cgi(cgi, cid, lac, operator, technology, codestation,
                                           cgiparagon, sitename, azimuth, date, azimuth2,
                                           address, localidade, concelho, distrito, codpostal, lat, lon, point)
                           SELECT DISTINCT cgi, cid, lac, operator, technology, codestation,
                                           cgiparagon, sitename, azimuth, date, azimuth2,
                                           address, localidade, concelho, distrito, codpostal, lat, lon, point
                           FROM temp
                           WHERE NOT EXISTS (
                                SELECT 'X' 
                                FROM cgi
                                WHERE 
                                    temp.cgi = cgi.cgi
                                    AND temp.cid = cgi.cid
                                    AND temp.lac = cgi.lac
                                    AND temp.operator = cgi.operator
                                    AND temp.technology = cgi.technology
                                    AND temp.codestation = cgi.codestation
                                    AND temp.cgiparagon = cgi.cgiparagon
                                    AND temp.sitename = cgi.sitename
                                    AND temp.azimuth = cgi.azimuth
                                    AND temp.date = cgi.date
                                    AND temp.azimuth2 = cgi.azimuth2
                                    AND temp.address = cgi.address
                                    AND temp.localidade = cgi.localidade
                                    AND temp.concelho = cgi.concelho
                                    AND temp.distrito = cgi.distrito
                                    AND temp.codpostal = cgi.codpostal
                                    AND temp.lat = cgi.lat
                                    AND temp.lon = cgi.lon
                                    AND temp.point = cgi.point
                            );";
        #endregion

        #region Methods
        public string updateDB(string csvPath)
        {          
            try
            {
                // Create temporary table -------------------------------------
                DaoDB.executeCommand(createTemp);

                // Insert data into temporary table ---------------------------
                DaoDB.executeCommand("COPY temp FROM '" + csvPath + "' (DELIMITER ',', HEADER TRUE, FORMAT CSV);");

                // Update points (geography) in temporary table ---------------
                DaoDB.executeCommand(updateGeoPoints);

                // insert the updated values to the DB ------------------------
                insertedRows = DaoDB.executeCommand(insertValues);

                // Drop temporary table ---------------------------------------
                DaoDB.executeCommand("DROP TABLE temp;");
            }
            catch (Exception)
            {
                throw;
            }

            // Close the connection to the DB
            DaoDB.CloseDbConnection();

            if (string.IsNullOrEmpty(insertedRows)) { insertedRows = "0"; }
            result = "Base de dados actualizada com sucesso!\nInseridas " + insertedRows + " novas linhas.";

            return result;
        }
        #endregion
    }
}
