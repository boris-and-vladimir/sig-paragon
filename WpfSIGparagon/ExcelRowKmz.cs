﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfSIGparagon
{
    class ExcelRowKmz
    {
        #region Properties and Fields
        // titles -------------------------------------------------------------
        private List<string> titlesRow { get; set; }

        // obrigatory values --------------------------------------------------
        private string latitude { get; set; }

        private string longitude { get; set; }

        private string name { get; set; }

        private string description { get; set; }

        // optional values ---------------------------------------------------

        private string appenddatacolumnstodescription { get; set; }

        private string foto { get; set; }

        private List<string> otherVals { get; set; }

        // icon values --------------------------------------------------------
        private string icon { get; set; }

        private string iconcolor { get; set; }

        private string iconscale { get; set; } 

        private string iconheading { get; set; }

        private string linestringcolor { get; set; }
        
        // polygon values -----------------------------------------------------
        private string polygon { get; set; }

        private string polygoncolor { get; set; }

        private string polygonaltitude { get; set; }

        private string polygonazimute { get; set; }

        private string polygonamplitude { get; set; }


        // square values ------------------------------------------------------
        private string squarealtitude { get; set; }

        private string squarelatitude { get; set; }

        private string squarelongitude { get; set; }

        private string squarecolor { get; set; }

        
        // helper values ------------------------------------------------------
        private List<string> officialVals = new List<string>{ "latitude", "longitude",
            "name", "description", "appenddatacolumnstodescription",  "foto",
            "icon", "iconcolor", "iconscale",  "iconheading", "linestringcolor",
            "polygon", "polygoncolor", "polygonaltitude", "polygonazimute", "polygonamplitude",
            "squarealtitude", "squarelatitude", "squarelongitude", "squarecolor"};

        private List<bool> rowHasVals { get; set; } // construir com a titlesRow

        private bool hasOtherVals { get; set; } // para os valores não oficiais
        #endregion


        #region Methods
        #region Constructor
        public void excelRowKmz(List<string> row, bool newWorksheet)
        {
            if (newWorksheet) // first row (titles)
            {
                foreach (string val in row) // build titlesRow
                {
                    titlesRow.Add(val.ToLower());
                }

                foreach( string val in officialVals ) // build rowHas Vals
                {
                    if (titlesRow.Contains(val)) { rowHasVals.Add(true); }
                    else { rowHasVals.Add(false); }
                }

                // ATENÇÂO -> Aqui tenho de verificar se o Excel tem mais colunas não oficiais e construir a otherVals
                foreach( string val in titlesRow)
                {
                    if (!officialVals.Contains(val))
                    {
                        otherVals.Add(val);
                    }
                }
                if ( otherVals.Count > 0) { hasOtherVals = true; }
                else { hasOtherVals = false; }
            }
            else // next rows (values)
            {
                // obrigatory values ------------------------------------------
                latitude = row[0];
                longitude = row[1];
                name = row[2];
                description = row[3];

                // optional values -------------------------------------------
                if (rowHasVals[officialVals.IndexOf("appenddatacolumnstodescription")] &&
                    row[titlesRow.IndexOf("appenddatacolumnstodescription")] != "")
                {
                    appenddatacolumnstodescription = row[titlesRow.IndexOf("appenddatacolumnstodescription")];
                }
                if (rowHasVals[officialVals.IndexOf("foto")] &&
                    row[titlesRow.IndexOf("foto")] != "")
                {
                    foto = row[titlesRow.IndexOf("foto")];
                }

                // icon values ------------------------------------------------
                if (rowHasVals[officialVals.IndexOf("icon")] && 
                    row[titlesRow.IndexOf("icon")] != "")
                {
                    icon = row[titlesRow.IndexOf("icon")];
                }
                if (rowHasVals[officialVals.IndexOf("iconcolor")] &&
                    row[titlesRow.IndexOf("iconcolor")] != "")
                {
                    iconcolor = row[titlesRow.IndexOf("iconcolor")];
                }
                if (rowHasVals[officialVals.IndexOf("iconscale")] &&
                    row[titlesRow.IndexOf("iconscale")] != "")
                {
                    iconscale = row[titlesRow.IndexOf("iconscale")];
                }
                if (rowHasVals[officialVals.IndexOf("iconheading")] &&
                    row[titlesRow.IndexOf("iconheading")] != "")
                {
                    iconheading = row[titlesRow.IndexOf("iconheading")];
                }
                if (rowHasVals[officialVals.IndexOf("linestringcolor")] &&
                    row[titlesRow.IndexOf("linestringcolor")] != "")
                {
                    iconheading = row[titlesRow.IndexOf("linestringcolor")];
                }

                // polygon values --------------------------------------------
                if (rowHasVals[officialVals.IndexOf("polygon")] &&
                    row[titlesRow.IndexOf("polygon")] != "")
                {
                    polygon = row[titlesRow.IndexOf("polygon")];
                }
                if (rowHasVals[officialVals.IndexOf("polygoncolor")] &&
                    row[titlesRow.IndexOf("polygoncolor")] != "")
                {
                    polygoncolor = row[titlesRow.IndexOf("polygoncolor")];
                }
                if (rowHasVals[officialVals.IndexOf("polygonaltitude")] &&
                    row[titlesRow.IndexOf("polygonaltitude")] != "")
                {
                    polygonaltitude = row[titlesRow.IndexOf("polygonaltitude")];
                }
                if (rowHasVals[officialVals.IndexOf("polygonazimute")] &&
                    row[titlesRow.IndexOf("polygonazimute")] != "")
                {
                    polygonazimute = row[titlesRow.IndexOf("polygonazimute")];
                }
                if (rowHasVals[officialVals.IndexOf("polygonamplitude")] &&
                    row[titlesRow.IndexOf("polygonamplitude")] != "")
                {
                    polygon = row[titlesRow.IndexOf("polygonamplitude")];
                }

                // square values ----------------------------------------------
                if (rowHasVals[officialVals.IndexOf("squarealtitude")] &&
                    row[titlesRow.IndexOf("squarealtitude")] != "")
                {
                    squarealtitude = row[titlesRow.IndexOf("squarealtitude")];
                }
                if (rowHasVals[officialVals.IndexOf("squarelatitude")] &&
                    row[titlesRow.IndexOf("squarelatitude")] != "")
                {
                    squarelatitude = row[titlesRow.IndexOf("squarelatitude")];
                }
                if (rowHasVals[officialVals.IndexOf("squarelongitude")] &&
                    row[titlesRow.IndexOf("squarelongitude")] != "")
                {
                    squarelongitude = row[titlesRow.IndexOf("squarelongitude")];
                }
                if (rowHasVals[officialVals.IndexOf("squarecolor")] &&
                    row[titlesRow.IndexOf("squarecolor")] != "")
                {
                    squarecolor = row[titlesRow.IndexOf("squarecolor")];
                }
            }
        }
        #endregion

        #endregion

    }
}
